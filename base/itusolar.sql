-- Suivie d'energie
create table type (
    id integer primary key not null,
    titre varchar2(20) not null
);
create sequence s_type increment by 1;


create table source (
    id integer primary key not null,
    titre varchar2(30) not null,
    typeid integer
);
create sequence s_source increment by 1;
alter table source add foreign key (typeid) references type(id);

create table HistEnergie(
    id integer primary key not null,
    sourceid integer,
    tensionentre number(12,2) DEFAULT 0 not null ,
    tensionsortie number(12,2) DEFAULT 0 not null ,
    intensiteentree number(12,2) DEFAULT 0 not null ,
    intensitesortie number(12,2) DEFAULT 0 not null ,
    puissancebatt number(12,2) DEFAULT 0 not null ,
    dateins timestamp default current_timestamp not null
);
create sequence s_HistEnergie increment by 1;
alter table HistEnergie add foreign key (sourceid) references source(id);

create table utilisateur (
    id integer primary key not null,
    nom varchar2(40),
    prenom varchar2(40),
    niveau integer default 0 not null,
    email varchar2(50),
    mdp varchar2(32) not null
);
create sequence s_utilisateur increment by 1;

create table Coupure (
    id integer primary key not null,
    sourceid integer,
    debut timestamp,
    fin timestamp,
    utilisateurid integer
);
alter table Coupure add foreign key (sourceid) references source(id);
alter table Coupure add foreign key (utilisateurid) references utilisateur(id);
create sequence s_coupure;

create table Tarifjirama (
    id integer primary key not null,
    dateins date,
    utilisateurid integer,
    compteur number(12,2),
    valuer number(8,2)
);
alter table Tarifjirama add foreign key (utilisateurid) references utilisateur(id);
create sequence s_Tarifjirama;

create table TaxeJirama (
    id integer primary key not null,
    titre varchar2(30),
    pourcentage number(5,2)
);
create sequence s_taxeJirama;
-- Suivie d'energie

-- Simulation
create table appareil (
    id integer primary key not null,
    titre varchar2(50),
    consommation number(10,2) default 0 not null
);
create sequence s_appareil;

create table simulation (
    id integer primary key not null,
    titre varchar2(50),
    utilisateurid integer,
    dateins date default current_date,
    seuilalerte number(12,2) default 0 not null
);
alter table simulation add foreign key (utilisateurid) references utilisateur(id);
create sequence s_simulation;

create table simuldetails (
    id integer primary key not null ,
    simulationid integer,
    nombre number(20,2) default 0 not null,
    appareilid integer,
    debut timestamp,
    fin timestamp
);
alter table simuldetails add foreign key (appareilid) references appareil(id);
alter table simuldetails add foreign key (simulationid) references simulation(id);
create sequence s_simuldetails;

-- Fin Simulation

-- Rapport
-- etat : 0 => gain (optimisation calcul ammortissement)
--        50 => depense (calcul cout d'installation)
create table depensegain (
    id integer primary key not null,
    valeur number(15,2) default 0 not null ,
    dateins date,
    etat integer default 0
);
create sequence s_depensegain;

-- ajout temperature histenergie
alter table HistEnergie add temperature number(6,2) default 0 not null;
-- Fin Rapport

-- View
create view v_histenergie(id,sourceid,tensionentre,tensionsortie,intensiteentree,intensitesortie,puissancebatt,dateins,
                          sc,mn,hr,dy,mt,yr,puissanceentree,puissancesortie,temperature
    ) as
select id,
       sourceid,
       tensionentre,
       tensionsortie,
       intensiteentree,
       intensitesortie,
       puissancebatt,
       dateins,
       extract(second from dateins) sc,
       extract(minute from dateins) mn,
       extract(hour from dateins) hr,
       extract(day from dateins) dy,
       extract(month from dateins) mt,
       extract(year from dateins) yr,
       HistEnergie.tensionentre*HistEnergie.intensiteentree puissanceentree,
       HistEnergie.tensionsortie*HistEnergie.intensitesortie puissancesortie,
       HistEnergie.temperature
from HISTENERGIE;

create view tablebord(sourceid,hr,dy,mt,yr,tensionentre,tensionsortie,intensiteentree,intensitesortie,puissancebatt,puissanceentree,puissancesortie,temperature) as
select he.sourceid,
       he.hr,
       he.dy,
       he.mt,
       he.yr,
       he.mn,
       sum(he.tensionentre) as tensionentre,
       sum(he.tensionsortie) as tensionsortie,
       sum(he.intensiteentree) as intensiteentree,
       sum(he.intensitesortie) as intensitesortie,
       sum(he.puissancebatt) as puissancebatt,
       sum(he.puissanceentree) as puissanceentree,
       sum(he.puissancesortie) as puissancesortie,
       avg(he.temperature) as temperature
from v_histenergie he
group by he.sourceid,he.yr,he.mt,he.dy,he.hr,he.mn;

-- Data
insert into type (id, titre)
values (s_type.nextval,'onduleur');

insert into source (id, titre, typeid)
values (s_source.nextval,'ond_1',2);

-- Fin data
