import { lazy } from 'react';
import { Navigate } from 'react-router-dom';
import AuthGuard from './auth/AuthGuard';
import { authRoles } from './auth/authRoles';
import Loadable from './components/Loadable';
import MatxLayout from './components/MatxLayout/MatxLayout';
import materialRoutes from 'app/views/material-kit/MaterialRoutes';

// session pages
const NotFound = Loadable(lazy(() => import('app/views/sessions/NotFound')));
const JwtLogin = Loadable(lazy(() => import('app/views/sessions/JwtLogin')));
const JwtRegister = Loadable(lazy(() => import('app/views/sessions/JwtRegister')));
const ForgotPassword = Loadable(lazy(() => import('app/views/sessions/ForgotPassword')));

// echart page
const AppEchart = Loadable(lazy(() => import('app/views/charts/echarts/AppEchart')));

// dashboard page
const Analytics = Loadable(lazy(() => import('app/views/dashboard/Analytics')));
const Consommation = Loadable(lazy(() => import('app/views/dashboard/Consommation')));
const Prediction = Loadable(lazy(() => import('app/views/dashboard/Prediction')));
const Simulation = Loadable(lazy(() => import('app/views/dashboard/Simulation')));
const Tarif = Loadable(lazy(() => import('app/views/jirama/Tarif')))
const Source = Loadable(lazy(() => import('app/views/source/Source')))
const Historique = Loadable(lazy(() => import('app/views/historique/Historique')))
const SimulationList = Loadable(lazy(() => import('app/views/dashboard/SimulationList')))
const SimulationGetter = Loadable(lazy(() => import('app/views/dashboard/SimulationGetter')))
const AddSource = Loadable(lazy(()=>import('app/views/source/AddSource')))
const TarifForm = Loadable(lazy(()=>import('app/views/jirama/TarifForm')))
const TaxeForm = Loadable(lazy(()=> import('app/views/jirama/TaxeForm')))
const Material = Loadable(lazy(()=> import('app/views/material/Material')))
const MaterialForm = Loadable(lazy(()=>import('app/views/material/MaterialForm')))
const Lieu = Loadable(lazy(()=>import('app/views/lieu/Building')))
const LieuForm = Loadable(lazy(()=>import('app/views/lieu/BuildingForm')))
const Battery = Loadable(lazy(()=>import('app/views/battery/Battery')))
const BatteryForm = Loadable(lazy(()=>import('app/views/battery/BatteryForm')))
const InverterState = Loadable(lazy(()=>import('app/views/dashboard/shared/InverterState')))
const Evolution = Loadable(lazy(()=>import('app/views/dashboard/shared/Evolution')))
const Repartition = Loadable(lazy(()=>import('app/views/dashboard/shared/Repartition')))
const Comparaison = Loadable(lazy(()=>import('app/views/dashboard/shared/Comparaison')))
const PredictionPdf = Loadable(lazy(()=>import('app/views/dashboard/PredictionPdf')))

const routes = [
  {
    element: (
      <AuthGuard>
        <MatxLayout />
      </AuthGuard>
    ),
    children: [
      ...materialRoutes,
      // dashboard route
      {
        path: '/jirama/history',
        element: <Historique />,
        auth: authRoles.admin
      },
      {
        path: '/jirama/sources',
        element: <Source />,
        auth: authRoles.admin
      },
      {
        path: '/material',
        element: <Material />,
        auth: authRoles.admin
      },
      {
        path: '/battery',
        element: <Battery />,
        auth: authRoles.admin
      },
      {
        path: '/battery/form/:id',
        element: <BatteryForm />,
        auth: authRoles.admin
      },
      {
        path: '/material/form/:id',
        element: <MaterialForm />,
        auth: authRoles.admin
      },
      {
        path: '/jirama/sources/form',
        element: <AddSource />,
        auth: authRoles.admin
      },
      {
        path: '/jirama/pagetarif',
        element: <Tarif />,
        auth: authRoles.admin
      },
      {
        path: '/places',
        element: <Lieu />,
        auth: authRoles.admin
      },
      {
        path: '/places/form/:id',
        element: <LieuForm />,
        auth: authRoles.admin
      },
      {
        path: '/jirama/pagetarif/form',
        element: <TarifForm />,
        auth: authRoles.admin
      },
      {
        path: '/jirama/taxe/form/:id',
        element: <TaxeForm />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/default',
        element: <Analytics />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/evolution',
        element: <Evolution  />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/comparaison',
        element: <Comparaison  />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/repartition',
        element: <Repartition  />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/predictions',
        element: <Prediction />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/predictions/pdf',
        element: <PredictionPdf />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/section/:id',
        element: <InverterState />,
        auth: authRoles.admin
      },
      {
        path: '/details/consommation',
        element: <Consommation />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/simulation',
        element: <Simulation  scheduleId={-100}  />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/simulation/:id',
        element: <SimulationGetter   />,
        auth: authRoles.admin
      },
      {
        path: '/dashboard/simulation/list',
        element: <SimulationList   />,
        auth: authRoles.admin
      },

      // e-chart rooute
      {
        path: '/charts/echarts',
        element: <AppEchart />,
        auth: authRoles.editor
      }
    ]
  },

  // session pages route
  { path: '/session/404', element: <NotFound /> },
  { path: '/session/signin', element: <JwtLogin /> },
  { path: '/session/signup', element: <JwtRegister /> },
  { path: '/session/forgot-password', element: <ForgotPassword /> },

  { path: '/', element: <Navigate to="dashboard/default" /> },
  { path: '*', element: <NotFound /> }
];

export default routes;
