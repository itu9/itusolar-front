import HForm from "../components/HForm";
import React, {useEffect, useRef, useState} from "react";
import HTextField from "../components/HTextField";
import HButton from "../components/HButton";
import Heading from "../components/Heading";
import HAutoCompleted from "../components/HAutoCompleted";
import {baseUrl} from "../../utils/constant";
import {useParams} from "react-router-dom";
import Swal from "sweetalert2";

const BuildingForm = () => {
    const {id} = useParams();
    const title = useRef(null);
    const [utils, setUtils] = useState({
        sections : [],
        building : null
    });
    const [type, setType] = useState('');
    const [typeValue, setTypeValue] = useState(-1);
    const [sections, setSections] = useState([]);
    const save = () => {
        let params = {
            id : id,
            titre : title.current.value,
            sectionid : typeValue
        }
        console.log(params)
        let url = baseUrl+"/building/create"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                window.location.replace('/places')
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            })});;
    }
    const handleTypeChange = (index,event={target : {name : ''}}, value) => {
        let target = 'type'
        if (event.target.name === target) {
            setType(value)
        } else if (event.target.name === target+'Value') {
            setTypeValue(value)
        }
    }

    useEffect(() => {
        let url = baseUrl+"/building/utils"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                id : id
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log(response)
                setSections(response.sections)
                setUtils(response)
            });
    }, []);
    useEffect(() => {
        if (utils.building) {
            title.current.value = utils.building.titre;
            setType(utils.building.section)
            setTypeValue(utils.building.sectionid)
        }
    }, [utils]);

    return (
        <HForm title={"Ajouter ou modifier un batiment"}>
            <HTextField title={"Titre"} defaultValue={"Batiment"} inputRef={title}></HTextField>
            <Heading>Section</Heading>
            <HAutoCompleted
                index={0}
                width={'100%'}
                name={"type"}
                top={'0%'}
                datas={sections}
                value={type}
                label={"Sections"}
                handleChange={(index,event,value)=>{
                    handleTypeChange(index,event,value)
                }}
                removeOnVoid={(index) => {

                }}
                onClick={(index)=>{

                }}
            ></HAutoCompleted>
            <HButton title={"Enregistrer"} clickListener={save}></HButton>
        </HForm>
    )
}
export default BuildingForm;