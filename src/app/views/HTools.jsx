class HTools {
    static monthList = [
        {
            start : '-01-01',
            end : '-01-31'
        },
        {
            start : '-02-01',
            end : '-02-28'
        },
        {
            start: '-03-01',
            end: '-03-31'
        },
        {
            start: '-04-01',
            end: '-04-30'
        },
        {
            start: '-05-01',
            end: '-05-31'
        },
        {
            start: '-06-01',
            end: '-06-30'
        },
        {
            start: '-07-01',
            end: '-07-31'
        },
        {
            start: '-08-01',
            end: '-08-31'
        },
        {
            start: '-09-01',
            end: '-09-30'
        },
        {
            start: '-10-01',
            end: '-10-31'
        },
        {
            start: '-11-01',
            end: '-11-30'
        },
        {
            start: '-12-01',
            end: '-12-30'
        }
    ]
    static dashboardModel = {
        sectionStates : [],
        input : 0,
        output : 0,
        batteryState : 0,
        meteo : {
            temperature: 0,
            label : ''
        }
    }
    static statVoid = {
        solaires : [],
        jirama : [],
        dates : [],
        meteos : [],
        statistics : [],
        histMeans : [],
        section : [],
        heur : [],
        total : 0,
        notSupported : 0,
        supported : 0,
        facture : {
            totalNTaxe : 0,
            pTotal : 0,
            tarif : {
                compteur : 0,
                valeur : 0,
                taxes : []
            },
            detailFactures : [],
            representation : []
        },
        etatConsommations : [],
        conso : []
    }
    static formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'MGA',

        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
    static jusqueSeize = ["zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize"];
    // Liste des dizaines de 10 à 90
    static dizaines = ["rien", "dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante", "quatre-vingt", "quatre-vingt"];
    // Liste des milliers de 1000 à 1000000000000
    static milliers = ["", "mille", "million", "milliard", "billion"];

    static  dateFormater = new Intl.DateTimeFormat("fr-FR", { year: "numeric", month: "long", day: "2-digit" })
    static dateTimeFormatter = new Intl.DateTimeFormat('en-FR', { dateStyle: 'full', timeStyle: 'long' })

    static numberFormatter = new Intl.NumberFormat('fr-FR', {
        maximumFractionDigits : 2,
        minimumFractionDigits : 2
    })
    static formatDatetime = (value) => {
        if (value === null)
            return value
        value = value.split(".")[0]
        let splited = value.split(":")
        return splited[0]+":"+splited[1]
    }

    static formatDate = (value) => {
        value = Date.parse(value)
        return HTools.dateFormater.format(value)
    }
    static prepareDate = (value) => {
        let tab = value.split("T");
        return tab.join(" ")
    }
    static formatNumber = (value) => {
        return (value)?value.toLocaleString('fr-FR', {minimumFractionDigits: 2, maximumFractionDigits: 2}):value
    }
    static formatAriary = (value) => {
        let res = HTools.formatter.format(value);
        return res.split("MGA")[1]+" ariary"
    }
    static spellAriary = (nombre) => {
        let partieEntiere = Math.floor(nombre);
        let partieDecimale = nombre - partieEntiere;
        partieDecimale = partieDecimale *100;
        partieDecimale = parseInt(partieDecimale, 10)
        let afterV = (partieDecimale > 0.) ? HTools.nombreEnLettres(partieDecimale): '';
        afterV = afterV.toLowerCase()
        return HTools.nombreEnLettres(partieEntiere)+" Ariary "+afterV;
    }
    // Fonction qui convertit un nombre en lettres
    static nombreEnLettres = (nombre)   => {
    // Liste des nombres de 0 à 16
        // Variable qui contiendra le résultat en lettres
        var resultat = "";

        // On vérifie si le nombre est négatif, nul ou trop grand
        if (nombre < 0) {
            return "Nombre négatif non géré.";
        }
        if (nombre == 0) {
            return HTools.jusqueSeize[0];
        }
        if (nombre >= 1e15) {
            return "Nombre trop grand.";
        }

        // On sépare la partie entière et la partie décimale du nombre
        var partieEntiere = Math.floor(nombre);
        var partieDecimale = nombre - partieEntiere;

        // On traite la partie entière en la découpant en tranches de trois chiffres
        for (let i = HTools.milliers.length - 1; i >= 0; i--) {
            // On calcule le quotient et le reste de la division par le millier correspondant
            var quotient = Math.floor(partieEntiere / Math.pow(1000, i));
            var reste = partieEntiere % Math.pow(1000, i);

            // Si le quotient est supérieur à zéro, on ajoute sa transcription en lettres
            if (quotient > 0) {
                resultat += HTools.transcrire3Chiffres(quotient) + " ";
                // On ajoute le nom du millier correspondant, avec un s si besoin
                if (i > 1 && quotient > 1) {
                    resultat += HTools.milliers[i] + "s ";
                } else {
                    resultat += HTools.milliers[i] + " ";
                }
            }

            // On passe au reste pour la prochaine itération
            partieEntiere = reste;

            // On ajoute une liaison si besoin
            if (i > 0 && reste > 0 && reste < Math.pow(1000, i - 1) && resultat !== '') {
                if (reste < 100 && reste != 80 && reste != 60) {
                    resultat += "et ";
                } else {
                    resultat += "-";
                }
            }

            // On ajoute un s à quatre-vingt si besoin
            if (i == 0 && reste == 80) {
                resultat += "-s";
            }

        }

        // On supprime les espaces inutiles
        resultat = resultat.trim();

        // On met une majuscule au début
        resultat = resultat[0].toUpperCase() + resultat.slice(1);
        // On retourne le résultat
        return resultat;

    }

// Fonction qui transcrit trois chiffres en lettres
    static transcrire3Chiffres = (nombre) => {

        // Si le nombre est inférieur ou égal à seize, on retourne directement sa transcription
        if (nombre <= 16) {
            return HTools.jusqueSeize[nombre];
        }

        // Sinon, on initialise une variable pour stocker la transcription
        var transcription = "";

        // On sépare les centaines, les dizaines et les unités du nombre
        var centaines = Math.floor(nombre / 100);
        var dizaines = Math.floor((nombre % 100) / 10);
        var unites = nombre % 10;

        // Si le nombre a des centaines, on les transcrit en lettres
        if (centaines > 0) {
            if (centaines === 1) {
                transcription += "cent";
            } else {
                transcription += HTools.jusqueSeize[centaines] + " cents";
            }
            // On ajoute une liaison si besoin
            if (dizaines > 0 || unites > 0) {
                if (dizaines === 0 || dizaines === 8) {
                    transcription += "-";
                } else {
                    transcription += " ";
                }
            }
        }

        // Si le nombre a des dizaines, on les transcrit en lettres
        if (dizaines > 0) {
            if (dizaines === 7 || dizaines === 9) {
                console.log("dizaines")
                // Cas particuliers des nombres en 70 et 90
                transcription += HTools.dizaines[dizaines - 1];
                if (unites > 0) {
                    transcription += "-" + HTools.jusqueSeize[10 + unites];
                }
            } else {
                // Cas général
                transcription += HTools.dizaines[dizaines];
                if (unites > 0) {
                    if (unites === 1 && dizaines !== 8) {
                        transcription += "-et-";
                    } else {
                        transcription += "-";
                    }
                    transcription += HTools.jusqueSeize[unites];
                }
            }
        } else {
            // Si le nombre n'a pas de dizaines, on transcrit les unités en lettres
            if (unites > 0) {
                transcription += HTools.jusqueSeize[unites];
            }
        }

        // On retourne la transcription
        return transcription;
    }

}

export default HTools;