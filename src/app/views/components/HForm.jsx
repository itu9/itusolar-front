import {hc_dark} from "../../utils/constant";
import React from "react";
import {Grid, styled} from "@mui/material";

const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const HForm = ({title,children, size = 6}) => {
    return (
        <>
            <ContentBox className="analytics">
                <Grid container spacing={3}>
                    <Grid item lg={size} md={size} sm={12} xs={12}>
                        <h1 style={{color : hc_dark}}>{title}</h1>
                        {children}
                    </Grid>
                </Grid>
            </ContentBox>
        </>
    )
}
export default HForm;