import {Button, styled} from "@mui/material";
import React from "react";

const StyledButton = styled(Button)(({ theme }) => ({
    width: '100%'
}));
const HButton = ({title, clickListener, variant="contained",color="success",mTop = '20px'}) => {
    const getColor = (color) => {
        if (color) {
            if (color === 'success') {
                return '#02a158';
            }
        }
        return color;
    }

    return (
        <StyledButton variant={variant} color={color} onClick={clickListener} sx={{
            marginTop : mTop
        }}>
            {title}
        </StyledButton>
    )
}
export default HButton;