import {Paper, styled} from "@mui/material";


const HContainers = styled(Paper) (({ theme }) => ({
    padding: '30px',

}));
const HContainer = ({children}) => {
    return (
        <HContainers>
            {children}
        </HContainers>
    );
}
export default HContainer;