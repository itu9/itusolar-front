import {styled} from "@mui/material";

const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const HContentBox = ({children}) => {
    return (
        <ContentBox>
            {children}
        </ContentBox>
    )
}
export default HContentBox;