import {styled} from "@mui/material";

const H = styled("h2")(({ theme }) => ({
    textAlign: "center" ,
    color:"#1976d2"
}))
const HTitle = ({children}) => {
    return (
        <H>{children}</H>
    )
}
export default HTitle;