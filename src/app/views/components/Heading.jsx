import {styled} from "@mui/material";

const H = styled("h3")(({ theme }) => ({
    textAlign: "left" ,
    color:"#1976d2",
    fontSize:'20px'
}))
const Heading = ({children}) => {
    return (
        <H>{children}</H>
    )
}
export default Heading;