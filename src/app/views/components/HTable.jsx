import {styled, Table, TableBody, TableHead, TableRow} from "@mui/material";

const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const HTable = ({head,children}) => {
    return (
        <StyledTable>
            <TableHead>
                <TableRow >
                    {head}
                </TableRow>
            </TableHead>
            <TableBody>
                {children}
            </TableBody>
        </StyledTable>
    )
}
export default HTable;