import {styled, TextField} from "@mui/material";
import React from "react";
import Heading from "./Heading";

const HTextField = ({title, inputRef, defaultValue=''}) => {
    return (
        <>
            <Heading>{title}</Heading>
            <TextField
                style={{width:"100%"}}
                type="text"
                inputRef={inputRef}
                defaultValue={defaultValue}
            />
        </>
    )
}
export default HTextField;