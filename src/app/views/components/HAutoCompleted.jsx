import {Autocomplete, styled, TextField} from "@mui/material";
import React from "react";

const StyledAutoComplete = styled(Autocomplete) (({ theme }) =>({
    marginBottom: '16px',
}))
/*
* Il faut que le valiable value ne soit jamais null.
* */
const HAutoCompleted = ({index,name, datas, value,width='200px',top='9%',label = 'Combo Box',removeOnVoid=(index)=>{},onClick=(index)=>{},handleChange=(index,event,value)=>{}}) => {
    return (
        <StyledAutoComplete
            name={name}
            options={datas}
            getOptionLabel={(option) => option.label}
            style={{paddingTop:top,width:width}}
            inputValue={value}
            onChange={(event,value)=> {
                console.log('value',value)
                let eventTemp = {
                    target : {
                        name : name
                    }
                }
                let labelValue = value !== null ? value.label : ''
                handleChange(index,eventTemp,labelValue)
                eventTemp.target.name = eventTemp.target.name+"Value"
                handleChange(index,eventTemp,value?value.id : -1)
            }}
            renderInput={(params) => (
                <TextField
                    onBlur={() => {
                        removeOnVoid(index)
                    }}
                    onClick={() => {
                        onClick(index)

                    }}
                    name={name} {...params} label={label} variant="outlined"/>
            )}
        />
    );
}
export default HAutoCompleted;