import HForm from "../components/HForm";
import {Grid, TableCell, TableRow} from "@mui/material";
import HButton from "../components/HButton";
import React, {useEffect, useState} from "react";
import HContainer from "../components/HContainer";
import HTable from "../components/HTable";
import {baseUrl} from "../../utils/constant";

const Battery = () => {
    const [datas, setDatas] = useState({
        batteries : []
    });
    useEffect(() => {
        let url = baseUrl+'/battery/list/content'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);

    const update = (id) => {
        window.location.replace("/battery/form/"+id)
    }
    return (
        <HForm title={"Batteries existantes"} size={8}>

            <Grid item lg={12} md={12} sm={12} xs={12}>
                <HButton title={"Ajouter une batterie"} clickListener={
                    () => {
                        window.location.replace("/battery/form/-1"
                        )
                    }}/>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} spacing={3} sx={{marginTop : '30px'}}>
                <HContainer>
                    <HTable
                        head={<>
                            <TableCell align="left">Titre</TableCell>
                            <TableCell align="left">Consommation</TableCell>
                            <TableCell align="left"></TableCell>
                        </>}
                    >
                        {
                            datas.batteries.map((data,index) => (
                                <>
                                    <TableRow>
                                        <TableCell align="left">
                                            {data.titre}
                                        </TableCell>
                                        <TableCell align="left">
                                            {data.capacite}W
                                        </TableCell>
                                        <TableCell align="left">
                                            <HButton mTop={'0px'} title={"Modifier"} variant={"outlined"} color={"primary"}
                                                     clickListener={()=>{update(data.id)}}></HButton>
                                        </TableCell>
                                    </TableRow>
                                </>
                            ))
                        }
                    </HTable>
                </HContainer>
            </Grid>
        </HForm>
    )
}
export default Battery;