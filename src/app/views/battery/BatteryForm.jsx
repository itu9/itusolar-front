import HForm from "../components/HForm";
import HTextField from "../components/HTextField";
import {useEffect, useRef, useState} from "react";
import HButton from "../components/HButton";
import {useParams} from "react-router-dom";
import {baseUrl} from "../../utils/constant";
import Swal from "sweetalert2";

const BatteryForm = () => {
    const {id} = useParams();
    const title = useRef(null);
    const capacity = useRef(null);
    const [datas, setDatas] = useState({
        battery : null
    });
    const save = () => {
        let params = {
            id : id,
            titre : title.current.value,
            capacite : capacity.current.value
        }
        let url = baseUrl+'/battery/save'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                window.location.replace('/battery')
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            });
        });
    }

    useEffect(() => {
        let url = baseUrl+'/battery/form/content'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                id : id
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);
    useEffect(() => {
        if (datas.battery) {
            title.current.value = datas.battery.titre
            capacity.current.value = datas.battery.capacite
        }
    }, [datas]);

    return (
        <HForm title={"Ajouter ou modifier une batterie"}>
            <HTextField title={"Titre"} defaultValue={"Batterie"} inputRef={title}/>
            <HTextField title={"Capacité"} defaultValue={"0"} inputRef={capacity} />
            <HButton title={"Enregistrer"} clickListener={save}></HButton>
        </HForm>
    )
}
export default BatteryForm;