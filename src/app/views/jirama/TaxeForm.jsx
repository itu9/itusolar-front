import HForm from "../components/HForm";
import HTextField from "../components/HTextField";
import {useEffect, useRef, useState} from "react";
import HButton from "../components/HButton";
import {baseUrl} from "../../utils/constant";
import Swal from "sweetalert2";
import {useParams} from "react-router-dom";


const TaxeForm = () => {
    const percent = useRef(null);
    const title = useRef(null);
    const {id} = useParams();
    const [data, setData] = useState({taxe : null});
    const save = () => {
        let params = {
            id : id,
            titre : title.current.value,
            pourcentage : percent.current.value
        }
        let url = baseUrl+"/tarif/addTaxe"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                window.location.replace("/jirama/pagetarif")
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            })
        });
    }
    useEffect(() => {
        let url = baseUrl+"/tarif/taxe/utils"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                taxe : {
                    id : id
                }
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log(response)
                setData(response)
                // window.location.replace("/jirama/pagetarif")
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            })
        });
    }, []);

    useEffect(() => {
        if (data.taxe) {
            title.current.value = data.taxe.titre
            percent.current.value = data.taxe.pourcentage
        }
    }, [data]);


    return (
        <HForm title={"Ajouter un taxe"}>
            <>
                <HTextField title={"Titre"} inputRef={title}></HTextField>
                <HTextField title={"Pourcentage"} inputRef={percent}></HTextField>
                <HButton title={"Enregistrer"} clickListener={save} />
            </>
        </HForm>
    );
}
export default TaxeForm;