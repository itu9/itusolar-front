import {baseUrl, hc_dark} from "../../utils/constant";
import React, {useEffect, useRef, useState} from "react";
import {Button, Grid, styled, TextField} from "@mui/material";
import HForm from "../components/HForm";
import Swal from "sweetalert2";

const Heading = styled("h3")(({ theme }) => ({
    textAlign: "left" ,
    color:"#1976d2",
    fontSize:'20px'
}))
const StyledButton = styled(Button)(({ theme }) => ({
    width: '100%',
    marginTop : '20px'
}));
const TarifForm = () => {
    const kwPrice = useRef(null);
    const counter = useRef(null);
    const [datas, setDatas] = useState({
        tarif : {
            compteur : 0,
            valeur : 0
        }
    });
    const save = () => {
        let params = {
            compteur : counter.current.value,
            valeur : kwPrice.current.value,
            dateins : new Date()
        }
        let url = baseUrl+'/tarif/save'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then((response) => {
                window.location.replace('/jirama/pagetarif')
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            });
            })
    }
    useEffect(() => {
        let url = baseUrl+'/tarif/form'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then((response={tarif : {compteur : 0, valeur : 0}
        }) => {
            kwPrice.current.value = response.tarif.valeur;
            counter.current.value = response.tarif.compteur;
            setDatas(response)
        });
    }, []);

    return (
        <>
            <HForm title={"Modifier le tarif"}>
                <Heading>Prix de 1 kW(Ariary)</Heading>
                <TextField
                    style={{width:"100%"}}
                    type="text"
                    inputRef={kwPrice}
                    defaultValue={datas.tarif.valeur}
                />
                <Heading>Prix de location de compteur(Ariary)</Heading>
                <TextField
                    style={{width:"100%"}}
                    type="text"
                    inputRef={counter}
                    defaultValue={datas.tarif.compteur}
                />
                <StyledButton variant="contained" color="success" onClick={save}>
                    Modifier
                </StyledButton>
            </HForm>
        </>
    );
}
export default TarifForm;