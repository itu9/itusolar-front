import {
    Button,
    Grid,
    Paper,
    styled, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField,
    useTheme
} from '@mui/material';
import React, {Fragment, useEffect, useState} from 'react';
import HTools from "../HTools";
import {baseUrl} from "../../utils/constant";
import HButton from "../components/HButton";

const StyledButton = styled(Button)(({ theme }) => ({
    margin: theme.spacing(1),
}));
const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));

const HContainer = styled(Paper) (({ theme }) => ({
    paddingTop:'30px',
    paddingLeft:'30px',
    paddingRight:'30px',
    paddingBottom:'30px'
}));

const Tarif = () => {
    const [data,setData] =useState(null);
    useEffect(() => {
        let url = baseUrl+'/tarif/pages'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setData(response)
            });
    }, []);

    const update = (id) => {
        window.location.replace("/jirama/taxe/form/"+id)
    }

    return (
        <Fragment>
            <ContentBox className="analytics">
                <Grid container spacing={3}>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                    <h1>Page de tarif JIRAMA</h1>
                        <StyledButton variant="contained" color="success" onClick={
                            () => {
                                window.location.replace("/jirama/pagetarif/form"
                                )
                            }}>
                            Modifier le tarif
                        </StyledButton>
                        <StyledButton variant="contained" color="success" onClick={
                            () => {
                                window.location.replace("/jirama/taxe/form/-1"
                                )
                            }}>
                            Ajouter un taxe
                        </StyledButton>
                    </Grid>
                    <Grid item lg={8} md={8} sm={12} xs={12}>
                        <HContainer>
                            <h2>Prix de 1 kWh</h2>
                            <p> {data !== null && data !== undefined ? HTools.formatAriary(data.tarif.valeur) : 'chargement...'}</p>
                            <h2>Coût de location de compteur</h2>
                            <p> {data !== null && data !== undefined ? HTools.formatAriary(data.tarif.compteur) : 'chargement...'}</p>
                            <h2>Taxes</h2>
                            {
                                data !== null && data !== undefined ? <>
                                    <StyledTable>
                                        <TableHead>
                                            <TableRow >
                                                <TableCell align="left">Titre</TableCell>
                                                <TableCell align="left">Pourcentage</TableCell>
                                                <TableCell align="left"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                data.taxes !== null && data.taxes !== undefined ?
                                                    data.taxes.map((taxe) => (
                                                        <TableRow >
                                                            <TableCell align="left">
                                                                {taxe.titre}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {taxe.pourcentage}%
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                <HButton variant={"outlined"} color={"primary"} mTop={'0px'} title={"Modifier"}  clickListener={()=>{update(taxe.id)}}></HButton>
                                                            </TableCell>
                                                            {/*    <TableCe*/}
                                                        </TableRow>

                                                    )): <></>
                                            }
                                        </TableBody>
                                    </StyledTable>
                                </> : 'chargement...'
                            }
                        </HContainer>
                    </Grid>
                </Grid>
            </ContentBox>
        </Fragment>
    );
}

export default Tarif;