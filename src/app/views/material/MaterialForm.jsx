import HForm from "../components/HForm";
import {useParams} from "react-router-dom";
import HTextField from "../components/HTextField";
import React, {useEffect, useRef, useState} from "react";
import HButton from "../components/HButton";
import {baseUrl} from "../../utils/constant";
import Swal from "sweetalert2";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import HTable from "../components/HTable";
import {TableCell, TableRow, TextField} from "@mui/material";
import Heading from "../components/Heading";
import MaterialComposition from "./MaterialComposition";


const MaterialForm = () => {
    const {id} = useParams();
    const title = useRef(null);
    const consumption = useRef(null);
    const [defaultConso, changeDefaultConso] = useState(0)
    const [datas, setDatas] = useState({
        material : null,
        others : []
    });
    const [content, setContent] = useState([]);
    const [isComposition, setComposition] = useState(false);
    const save = () => {
        let params = {
            id : id,
            titre : title.current.value,
            consommation : consumption.current?consumption.current.value: 0,
            materialCompositions : content
        }
        let url = baseUrl+'/material/save'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                window.location.replace('/material')
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            });
        });
    }


    useEffect(() => {
        let url = baseUrl+'/material/form'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                id : id
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);

    useEffect(() => {
        if (datas.material) {
            setComposition(datas.material.composed)
            title.current.value = datas.material.titre
            consumption.current.value = datas.material.consommation
        }
        setContent(datas.others)
    }, [datas]);

    useEffect(() => {
        console.log(content)
    }, [content]);

    const active = (id,value) => {
        let results = [
            ...content
        ]
        results = results.filter(result => {
            if (result.id === id) {
                result.checked = value
            }
            return result
        })
        setContent(results)
    }

    const changeNumber = (id, nombre) => {
        let results = [
            ...content
        ]
        results = results.filter(result => {
            if (result.id === id) {
                result.nombre = nombre
            }
            return result
        })
        setContent(results)
    }

    const switchIt = () => {
        setComposition(!isComposition);
        if (datas.material)
            changeDefaultConso(datas.material.consommation)
    }
    return (
        <HForm title={"Ajouter ou modifier un materiel"}  size={8}>
            <HTextField title={"Titre"} inputRef={title}></HTextField>

            <FormControlLabel
                label="Composition"
                control={
                    <Switch checked={isComposition} onChange={() => {
                        switchIt()
                    }} value="checkedA" />
                }
            />
            {
                !isComposition ? <>
                    <HTextField title={"Consommation"} defaultValue={defaultConso} inputRef={consumption}></HTextField>
                </> : <>
                    <Heading>Compositions</Heading>
                    <HTable
                        head={<>
                            <TableCell align="left">Nombre</TableCell>
                            <TableCell align="left">Appareil</TableCell>
                            <TableCell align="left"></TableCell>
                        </>}
                    >
                        {
                            content.map((other,index) => (
                                <>
                                    <MaterialComposition data={other} active={active} handleNumber={changeNumber}/>
                                </>
                                ))
                        }
                    </HTable>
                </>
            }

            <HButton title={"Enregistrer"} clickListener={save}></HButton>
        </HForm>
    )
}
export default MaterialForm;