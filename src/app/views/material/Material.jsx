import HForm from "../components/HForm";
import {Grid, TableCell, TableRow} from "@mui/material";
import React, {useEffect, useState} from "react";
import HButton from "../components/HButton";
import HTable from "../components/HTable";
import HContainer from "../components/HContainer";
import {baseUrl} from "../../utils/constant";

const Material = () => {
    const [datas, setDatas] = useState({
        materials : []
    });
    useEffect(() => {
        let url = baseUrl+'/material/all'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);


    const update = (id) => {
        window.location.replace("/material/form/"+id)
    }
    return (
        <HForm title={"Appareils existants"} size={8}>
            <Grid item lg={12} md={12} sm={12} xs={12}>
                <HButton title={"Ajouter un appareil"} clickListener={
                    () => {
                        window.location.replace("/material/form/-1"
                        )
                    }}/>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} spacing={3} sx={{marginTop : '30px'}}>
                <HContainer>
                    <HTable
                        head={<>
                            <TableCell align="left">Titre</TableCell>
                            <TableCell align="left">Consommation</TableCell>
                            <TableCell align="left"></TableCell>
                        </>}
                    >
                        {
                            datas.materials.map((data,index) => (
                                <>
                                    <TableRow>
                                        <TableCell align="left">
                                            {data.titre}
                                        </TableCell>
                                        <TableCell align="left">
                                            {data.consommation}Wh
                                        </TableCell>
                                        <TableCell align="left">
                                            <HButton mTop={'0px'} title={"Modifier"} variant={"outlined"} color={"primary"}
                                                     clickListener={()=>{update(data.id)}}></HButton>
                                        </TableCell>
                                    </TableRow>
                                </>
                            ))
                        }
                    </HTable>
                </HContainer>
            </Grid>
        </HForm>
    )
}
export default Material;