import {TableCell, TableRow, TextField} from "@mui/material";
import React, {useRef, useState} from "react";
import Switch from "@mui/material/Switch";

const MaterialComposition = ({data,active,handleNumber}) => {
    const nombre = useRef(null);
    const [isIn, setIn] = useState(data.checked? 1 : 0);
    const handleChange = (id) => {
        let val = !isIn? 1 : 0
        setIn(val);
        active(id, val)
    }
    return (
        <>
            <TableRow>
                <TableCell align="left">
                    <TextField
                        style={{width:"100%"}}
                        type="text"
                        inputRef={nombre}
                        defaultValue={data.nombre}
                        onChange={(e) => {
                            handleNumber(data.id, nombre.current.value);
                        }}
                    />
                </TableCell>
                <TableCell align="left">
                    {data.material.label}
                </TableCell>
                <TableCell align="left">
                    <Switch checked={isIn} onChange={() => {handleChange(data.id)}} value="checkedA" />
                </TableCell>
            </TableRow>
        </>
    )
}
export default MaterialComposition;