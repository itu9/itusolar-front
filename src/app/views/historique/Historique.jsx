import {
    Autocomplete, Button,
    Card,
    Grid, Icon,
    IconButton,
    Paper,
    styled, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField,
    useTheme
} from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';
import React, {Fragment, useEffect, useRef, useState} from 'react';
import HTools from "../HTools";
import Loadable from "../../components/Loadable";
import {baseUrl} from "../../utils/constant";
import {Pagination} from "@mui/lab";
import { CSVLink } from "react-csv";

const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'scroll',
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));

const HContainer = styled(Paper) (({ theme }) => ({
    paddingTop:'30px',
    paddingLeft:'30px',
    paddingRight:'30px',
    paddingBottom:'30px'
}));

const Historique = () => {
    const { palette } = useTheme();
    const [data,setData] =useState(null);
    const [csvFile, setCsvFile] = useState(null);
    const [csvArray, setCsvArray] = useState([]);
    const min = useRef(null);
    const max = useRef(null);
    const csvDatas =[
        ["Source", "Intensité d'entrée", "Tension d'entrée", "Intensité de sortie", "Tension de sortie","Batterie","Température","Date"],
    ];

    const searchIt = () => {
        let minText = min.current.value !== ""?min.current.value+":00.000" : null
        let maxText = max.current.value !== ""?max.current.value+":00.000" : null
        minText = minText !== null ? HTools.prepareDate(minText) : null
        maxText = maxText !== null ? HTools.prepareDate(maxText) : null
        search(minText, maxText)
    }

    const search = (start,end) => {
        let url = baseUrl+'/historique/dashboard'
        let params = {
            start : start,
            end : end
        }
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setData(response)
            });
    }
    useEffect(() => {
        search(null,null)
    }, []);
    useEffect(() => {
        console.log('csvArray',csvArray)
        let datas = []
        let verif = true;
        for (let i=0; i < csvArray.length;i++) {
            let data = csvArray[i];
            if (verif) {
                verif = false;
                continue;
            }
            if (!data || data.length < csvDatas[0].length)
                continue;
            let temp = {
                sourceid : data[0],
                intensiteentree : data[1],
                tensionentre : data[2],
                intensitesortie : data[3],
                tensionsortie : data[4],
                puissancebatt : data[5],
                temperature : data[6],
                datins : formatDateTime(data[7]),
            }
            datas = [
                ...datas,
                temp
            ]
        }
        let paramData = {
            datas : datas
        }
        // console.log(paramData)
        let url = baseUrl+"/historique/multiple"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(paramData),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
        .then(response => {
            // window.location.reload()
        });
    }, [csvArray]);
    const formatDateTime = (value) => {
        if (value === '')
            return null;
        let spliteds  = value.split(" ")
        let answer = spliteds[0].split('/')
        let resp = answer[2]+"-"+answer[1]+"-"+answer[0]
        resp += " "+ spliteds[1]+":00.000"
        return resp
    }
    const processCSV = (str) => {
        // split the file data into rows from the newline character
        let rows = str.split("\n");
        // remove comma
        rows = rows.map(function (row) {
            row = row.replace(/"/g, "");
            row = row.replace(/\r/g, "");
            row = row.replace(/,/g, ".");
            return row.split(";")
        });
        setCsvArray(rows);
    };

    const handleFileUpload = () => {
        const file = csvFile;
        const reader = new FileReader();

        reader.onload =  (e) => {
            const text = e.target.result;
            processCSV(text);
        };

        reader.readAsText(file);
    };

    return (
        <Fragment>
            <ContentBox className="analytics">
                <Grid container spacing={3}>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <h1>Historique d'energie</h1>
                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <HContainer>
                            <Grid container spacing={3}>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                    <h3 >Importation de données</h3>
                                    <Grid item lg={6} md={6} sm={12} xs={12}>
                                        <TextField type={"file"} placeholder={"Import csv"}
                                                   style={{
                                                       marginTop:'10px'
                                                   }}
                                                   onChange={(e) => {
                                                       setCsvFile(e.target.files[0]);
                                                   }}
                                        ></TextField>
                                    </Grid>
                                    <Grid item lg={6} md={6} sm={12} xs={12}>
                                        <Button variant="contained"
                                                style={{
                                                    marginTop:'10px'
                                                }}>
                                            <CSVLink data={csvDatas} separator={";"} >Formulaire csv</CSVLink>
                                        </Button>
                                        <Button variant="contained" color="success"
                                                onClick={(e) => {
                                                    handleFileUpload();
                                                }}
                                                style={{
                                                    marginLeft:'10px',
                                                    marginTop:'10px'
                                                }}>
                                            Importer csv
                                        </Button>
                                    </Grid>
                                </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <h3 >Date minimum</h3>
                                <TextField
                                    label=""
                                    id="filled-start-adornment"
                                    sx={{  width: '100%' }}
                                    inputRef={min}
                                    InputProps={{
                                        startAdornment: <InputAdornment  position="start">date</InputAdornment>,
                                        type : 'datetime-local',

                                    }}
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <h3 >Date maximum</h3>
                                <TextField
                                    label=""
                                    id="filled-start-adornment"
                                    sx={{  width: '100%' }}
                                    inputRef={max}
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start">date</InputAdornment>,
                                        type : 'datetime-local',
                                    }}
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item lg={6} md={6} sm={12} xs={12}>
                                <Button variant="contained" color="success"
                                    onClick={searchIt}
                                >
                                    Rechercher
                                </Button>
                            </Grid>
                            </Grid>
                            <h2>Résultats</h2>
                            {
                                data !== null && data !== undefined ? <>
                                    <StyledTable>
                                        <TableHead>
                                            <TableRow >
                                                <TableCell align="left">Source</TableCell>
                                                <TableCell align="left">P. d'entree</TableCell>
                                                <TableCell align="left">P. de sortie</TableCell>
                                                <TableCell align="left">Batterie</TableCell>
                                                <TableCell align="left">Temperature</TableCell>
                                                <TableCell align="left">Date</TableCell>
                                                {/*<TableCell align="left"></TableCell>*/}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                data.histEnergy !== null && data.histEnergy !== undefined ?
                                                    data.histEnergy.map((source) => (
                                                        <TableRow >
                                                            <TableCell align="left">
                                                                {source.type} {source.titre}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {HTools.formatNumber(source.puissanceentree) }W
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {HTools.formatNumber(source.puissancesortie)}W
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {HTools.formatNumber(source.puissancebatt)}W
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {HTools.formatNumber(source.temperature)}°
                                                            </TableCell>
                                                            <TableCell align="left"  >
                                                                {HTools.formatDatetime(source.datins)}
                                                            </TableCell>
                                                            {/*    <TableCe*/}
                                                        </TableRow>

                                                    )): <></>
                                            }
                                        </TableBody>
                                    </StyledTable>
                                    <Pagination count={10} style={{
                                        marginTop : "40px",
                                        marginBottom : "30px"
                                    }
                                    } />
                                </> : 'chargement...'
                            }
                        </HContainer>
                    </Grid>
                </Grid>
            </ContentBox>
        </Fragment>
    );
}

export default Historique;