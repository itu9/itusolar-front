import React from 'react';
import {Card, CardContent, Icon} from "@mui/material";

// Définir un style personnalisé pour le composant
const styles = {
    root: {
        height: 100,
        margin: 1,
        // backgroundColor: '#f0f0f0',
    },
    title: {
        fontSize: '1.1rem',
        fontWeight: 'bold',
        marginTop : '10px',
        color: '#1976d2'
    },
    temp: {
        fontSize: '1rem',
        color: '#ff0000',
    },
};

// Créer un composant fonctionnel qui prend en paramètre la date et la température
const WeatherCard = ({ date, temp }) => {
    return (
        <Card style={styles.root}>
            <CardContent>
                <div style={styles.title} align="center">
                    {date}
                </div>
                <div style={styles.temp} align="center">
                    {temp}°C
                </div>
            </CardContent>
        </Card>
    );
};

export default WeatherCard;
