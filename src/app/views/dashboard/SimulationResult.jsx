import Modal from "@mui/material/Modal";
import Backdrop from "@mui/material/Backdrop";
import Fade from "@mui/material/Fade";
import {
    Autocomplete,
    Box, Button, Card,
    FormControl,
    Grid,
    Icon, IconButton,
    Paper, Select, styled,
    Tab, Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import HTools from "../HTools";
import React, {useEffect, useState} from "react";
import {Small} from "../../components/Typography";
import {TabContext, TabList, TabPanel} from "@mui/lab";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import {Bar} from "react-chartjs-2";
import Facture from "./Facture";
import WeatherCard from "./WeatherCard";
import { CategoryScale, Chart,LinearScale,BarElement } from "chart.js";
import {Legend, Tooltip} from "chart.js";

Chart.register(CategoryScale);
Chart.register(LinearScale);
Chart.register(BarElement);

Chart.register(Legend);
Chart.register(Tooltip);

const MoreTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    minWidth : 335,
    width: 'auto',
    size :"small",
    ariaLabel :"a dense table",
    // overflowX : "auto",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));

const StyledCard = styled(Card)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '24px !important',
    background: theme.palette.background.paper,
    [theme.breakpoints.down('sm')]: { padding: '16px !important' },
}));
const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    minWidth : 750,
    width: 'auto',
    size :"small",
    ariaLabel :"a dense table",
    // overflowX : "auto",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const AutoComplete = styled(Autocomplete)(() => ({
    width: 300,
    marginBottom: '16px',
}));
const BtnAddLigne = styled(Button)(({ theme }) => ({
    marginTop: theme.spacing(1),
}));
const StyledButton2 = styled(Button)(({ theme }) => ({
    marginLeft: theme.spacing(1),
    width : '45%'
}));

const StyledAutoComplete = styled(AutoComplete) (({ theme }) =>({
    width : '200px',
}))
const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};

const ContentBox2 = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    '& small': { color: theme.palette.text.secondary },
    '& .icon': { opacity: 0.6, fontSize: '44px', color: theme.palette.primary.main },
}));

const Heading = styled('h6')(({ theme }) => ({
    margin: 0,
    marginTop: '4px',
    fontSize: '14px',
    fontWeight: '500',
    color: theme.palette.primary.main,
}));
const sources = [
    {
        id : 0,
        label : 'Solaire'
    },
    {
        id : 50,
        label : 'JIRAMA'
    }
]

const SimulationResult = ({stat,utils,linkPdf="/dashboard/predictions"}) => {
    const [detailsopen, setDetailsopen] = React.useState(false);
    const [consommation,setConso] = React.useState([]);
    const [section, setSection] = React.useState(-1);
    const [alerter, setAlerter] = useState(false);
    const [heur, setHeur] = React.useState([]);
    const [statData,setStatData] = React.useState([]);
    const [value, setValue] = React.useState('1');
    const [debutState, setDebutState] = React.  useState(null);
    const [finState, setFinState] = React.useState(null);
    const [representation, setRepresentation] = React.useState(0);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [open, setOpen] = React.useState(false);
    function handleRepresentation(event) {
        setRepresentation(event.target.value)
    }

    const showMore = (etat) => {
        console.log('conso',etat.consommation)
        setConso([etat])
        setDetailsopen(true)
    }
    const handleTabsChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleSection = (event) => {
        setSection(event.target.value);
        // filterStat({statistics:filterBySectionId(event.target.value)})
    };
    function handleFin(value) {
        if (value === '') {
            setFinState(null)
        } else {
            setFinState(Date.parse(value))
        }
    }

    function handleDebut(value) {
        if (value === '') {
            setDebutState(null)
        } else {
            setDebutState(Date.parse(value))
        }
    }

    const filterBySection = (active=false) => {
        return filterBySectionId(section);
    }

    const filterBySectionId = (sectionId) => {
        let results = stat.statistics.filter(stat => {
            return parseInt(stat.section.id,10) === parseInt(sectionId,10)} );
        if (debutState !== null)
            results = results.filter(result => debutState <= Date.parse(result.concern.debut))
        if (finState !== null)
            results = results.filter(result => finState >= Date.parse(result.concern.debut))
        return results
    }

    const filterStatBar = () => {
        console.log("called filterStatBar")
        return filterStat({statistics :filterBySection(false)})
    }

    const filterStat = (response) => {
        console.log('response',response)
        let conso = response.statistics.map((res) => res.consommation)
        let consoSup = response.statistics.map((res) => res.consoSup)
        let productions = response.statistics.map((res) => res.production)
        let restes = response.statistics.map((res) => res.reste)
        let heurs = response.statistics.map((res) => HTools.formatDate(res.date) + " "+res.heur + ":00")
        setHeur(heurs)
        let result = [
            {
                label: 'Production en Watt(W)',
                data: productions,
                backgroundColor: 'rgba(59, 255, 133, 1)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
            },{
                label: 'Consommation totale en Watt(W)',
                data: conso,
                backgroundColor: 'rgba(245, 27, 17, 0.96)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
            },{
                label: 'Consommation supporté en Watt(W)',
                data: consoSup,
                backgroundColor: 'rgba(255, 92, 17, 0.96)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
            },
            {
                label: 'Reste en Watt(W)',
                data: restes,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1,
            }
        ]
        setStatData(result)
        return result
    }

    const options = {
        scales: {
            xAxes: [
                {
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: "number of cars",
                    }
                },
            ],
            yAxes: [
                {
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: "number of cars",
                    },
                    ticks: {
                        beginAtZero: true,
                    }
                },
            ]
        },
    };
    useEffect(() => {
        filterStatBar()
    }, [stat]);

    useEffect(()=> {
        console.log("bar charged")
        filterStatBar();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[section,debutState,finState])
    const data = [
        { label: 'Lundi, 26', temperature: 25 },
    ];
    useEffect(() => {
        if (utils.sections.length > 0) {
            setSection(utils.sections[0].id)
        }
    }, [utils]);
    // useEffect(() => {
    //     if (representation >= 50) {
    //         filterStatBar();
    //     }
    // }, [representation]);


    return (
        <>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={detailsopen}
                onClose={() => {setDetailsopen(false)}}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                    backdrop: {
                        timeout: 500,
                    },
                }}
            >
                <Fade  in={detailsopen}>
                    <Box sx={styleModal}>
                        {
                            consommation.map((cons,index2) => (
                                < >
                                    <h1 >Appareils non supportés</h1>
                                    <Grid item lg={12} md={12} sm={12} xs={12}>
                                        <Grid item lg={4} md={4} sm={4} xs={4} >
                                            Début :
                                        </Grid>
                                        <Grid item lg={8} md={8} sm={8} xs={8} >
                                            {HTools.formatDatetime(cons.debut) }
                                        </Grid>
                                        <Grid item lg={6} md={6} sm={6} xs={6} >
                                            Fin :
                                        </Grid>
                                        <Grid item lg={6} md={6} sm={6} xs={6} >
                                            {HTools.formatDatetime(cons.fin)}
                                        </Grid>
                                    </Grid>
                                    <h1> </h1>
                                    <TableContainer>
                                        <MoreTable>
                                            <TableHead>
                                                <TableRow >
                                                    <TableCell align="left">Appareil</TableCell>
                                                    <TableCell align="left">Nombre</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <>
                                                    {
                                                        cons.consommation.map((detail,index) => (
                                                            <TableRow >
                                                                <TableCell align="left">
                                                                    {detail.appareil.label}
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {detail.nb}
                                                                </TableCell>
                                                            </TableRow>
                                                        ))
                                                    }
                                                </>
                                            </TableBody>
                                        </MoreTable>
                                    </TableContainer>
                                </>
                            ))
                        }

                    </Box>
                </Fade>
            </Modal>
            <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Meteo selon Open-Meteo.com</h2>
            <Grid container spacing={3} style={{ marginTop: '24px' }}>

                {stat.meteos.map((item,index) => (
                    <Grid item xs={12} md={parseInt((12/stat.meteos.length < 3.)? ((index < 4)?3 : 4) :(12/stat.meteos.length),10)}  key={index}>
                        <WeatherCard date={item.label} temp={item.temperature} />
                    </Grid>
                ))}
            </Grid>
            <Grid container spacing={3} style={{ marginTop: '24px' }}>
                <Grid item xs={12} md={4}>
                    <StyledCard elevation={6}>
                        <ContentBox2>
                            <Icon className="icon">highlight</Icon>
                            <Box ml="12px">
                                <Small>Consommation totale</Small>
                                <Heading>{HTools.formatNumber(stat.total)}w</Heading>
                            </Box>
                        </ContentBox2>
                    </StyledCard>
                </Grid>
                <Grid item xs={12} md={4}>
                    <StyledCard elevation={6}>
                        <ContentBox2>
                            <Icon className="icon">highlight</Icon>
                            <Box ml="12px">
                                <Small>Consommation supportée</Small>
                                <Heading>{HTools.formatNumber(stat.supported)}w</Heading>
                            </Box>
                        </ContentBox2>
                    </StyledCard>
                </Grid>
                <Grid item xs={12} md={4}>
                    <StyledCard elevation={6}>
                        <ContentBox2>
                            <Icon className="icon">highlight</Icon>
                            <Box ml="12px">
                                <Small>Consommation non supportée</Small>
                                <Heading>{HTools.formatNumber(stat.notSupported)}</Heading>
                            </Box>
                        </ContentBox2>
                    </StyledCard>
                </Grid>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} >
                <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>
                    <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>
                        <TabContext value={value}>
                            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                <TabList onChange={handleTabsChange} aria-label="tabs example" variant="fullWidth">
                                    <Tab label="Etat de consommation" value="1" />
                                    <Tab label="Production, consommation et reste d'énergie par heur" value="2" />
                                    <Tab label="Facturation basée sur le tarif de Jirama" value="3"  />
                                </TabList>
                            </Box>
                            <TabPanel value="1">
                                <Grid item lg={12} md={12} sm={12} xs={12} >
                                    <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Etat de consommation</h2>
                                    <FormControl sx={{ mt: 1, minWidth: 200}} size="small">
                                        <InputLabel id="demo-select-small-label">Etat</InputLabel>
                                        <Select
                                            labelId="demo-select-small-label"
                                            id="demo-select-small"
                                            label="Section"
                                            value={section}
                                            onChange={handleSection}
                                        >
                                            <MenuItem value={100}>Les deux</MenuItem>
                                            <MenuItem value={50}>Excés</MenuItem>
                                            <MenuItem value={0}></MenuItem>
                                        </Select>

                                    </FormControl>
                                    <TableContainer>
                                        <StyledTable style={{minWidth : 750,width: '100%'}}>
                                            <TableHead>
                                                <TableRow >
                                                    <TableCell align="left">Consommation</TableCell>
                                                    <TableCell align="left">Supportée</TableCell>
                                                    <TableCell align="left">Valeur</TableCell>
                                                    <TableCell align="left">Etat</TableCell>
                                                    <TableCell align="left">Debut</TableCell>
                                                    <TableCell align="left">Fin</TableCell>
                                                    <TableCell align="left"></TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    stat.etatConsommations.map((etats,index) => (
                                                        <>
                                                            {
                                                                etats.map((etat,index1) => (
                                                                    <TableRow  key={index1}>
                                                                        <TableCell align="left">
                                                                            {etat.etat ===0?<>{HTools.formatNumber(etat.conso) }w</> : <></>}
                                                                        </TableCell>
                                                                        <TableCell align="left">
                                                                            {etat.etat ===0?<>{HTools.formatNumber(etat.support)}w</> : <></>}
                                                                        </TableCell>
                                                                        <TableCell align="left">
                                                                            {HTools.formatNumber(etat.value)}w
                                                                        </TableCell>
                                                                        <TableCell align="left">

                                                                                    <span style={{  color: etat.etat ===0 ?"#F51B11" :"#1976d2"}}>
                                                                                        {etat.etat ===0? "Déficit" : "Excés"}
                                                                                    </span>
                                                                        </TableCell>
                                                                        <TableCell align="left">
                                                                            {HTools.formatDatetime(etat.debut)}
                                                                        </TableCell>

                                                                        <TableCell align="left">
                                                                            {HTools.formatDatetime(etat.fin)}
                                                                        </TableCell>
                                                                        <TableCell align="left">
                                                                            {etat.etat ===0?
                                                                                <IconButton onClick={() => {showMore(etat)}}>
                                                                                    <Icon color="default">remove_red_eye</Icon>
                                                                                </IconButton>:<></>
                                                                            }
                                                                        </TableCell>
                                                                    </TableRow>
                                                                ))
                                                            }
                                                        </>

                                                    ))
                                                }
                                            </TableBody>
                                        </StyledTable>
                                    </TableContainer>
                                </Grid>
                            </TabPanel>
                            <TabPanel value="2">
                                <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Production, consommation et reste d'énergie par heur</h2>
                                <FormControl sx={{ mt: 1, minWidth: 200}} size="small">
                                    <InputLabel id="demo-select-small-label">Section</InputLabel>
                                    <Select
                                        labelId="demo-select-small-label"
                                        id="demo-select-small"
                                        label="Section"
                                        value={section}
                                        onChange={handleSection}
                                    >
                                        {
                                            utils.sections.map((section,index) => (
                                                <MenuItem value={section.id}>{section.titre}</MenuItem>
                                            ))
                                        }
                                    </Select>

                                </FormControl>
                                <FormControl sx={{ mt: 1, minWidth: 200}} size="small">
                                    <InputLabel id="demo-select-small-label">Représentation</InputLabel>
                                    <Select
                                        labelId="demo-select-small-label"
                                        id="demo-select-small"
                                        label="Section"
                                        value={representation}
                                        onChange={handleRepresentation}
                                    >
                                        <MenuItem value="0">Représentation numérique</MenuItem>
                                        <MenuItem value="50">Représentation graphique</MenuItem>
                                        <MenuItem value="100">Les deux</MenuItem>

                                    </Select>
                                </FormControl>

                                <div sx={{ mt: 3, minWidth: 200}}>
                                    <InputLabel id="demo-select-small-label" style={{fontSize : '17px'}}  >Début</InputLabel>
                                    <input type={"datetime-local"}
                                           onChange={event => {
                                               handleDebut(event.target.value)
                                           }
                                           }
                                           name={"debut"} style={{fontSize: '15px',width : '250px',height:'40px'}}/>
                                </div>
                                <div sx={{ mt: 3, minWidth: 200}}>
                                    <InputLabel id="demo-select-small-label" style={{fontSize : '17px'}} >Fin</InputLabel>
                                    <input type={"datetime-local"}
                                           onChange={event => {
                                               handleFin(event.target.value)
                                           }
                                           }   name={"debut"} style={{fontSize: '15px',width : '250px',height:'40px'}}/>
                                </div>
                                {
                                    representation == 0 || representation >= 100 ?
                                        <TableContainer>
                                            <h2 style={{ textAlign: "left" }}  sx={{mt : 10}}>Représentation numérique</h2>
                                            <StyledTable style={{minWidth : 750,width: '100%'}} sx={{mb : 10}}>
                                                <TableHead>
                                                    <TableRow >
                                                        <TableCell align="left">Section</TableCell>
                                                        <TableCell align="left">Date</TableCell>
                                                        <TableCell align="left">Heur</TableCell>
                                                        <TableCell align="left">Production</TableCell>
                                                        <TableCell align="left">Consommation</TableCell>
                                                        <TableCell align="left">Supporté</TableCell>
                                                        <TableCell align="left">Reste</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        filterBySection(true).map((statistic,index) => (
                                                            <TableRow key={index}>
                                                                <TableCell align="left">
                                                                    {statistic.section.titre}
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {HTools.formatDate(statistic.date)}
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {statistic.heur+":00"}
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {HTools.formatNumber(statistic.production) }W
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {HTools.formatNumber(statistic.consommation)}W
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {HTools.formatNumber(statistic.consoSup)}W
                                                                </TableCell>
                                                                <TableCell align="left">
                                                                    {HTools.formatNumber(statistic.reste)}W
                                                                </TableCell>
                                                            </TableRow>
                                                        ))
                                                    }
                                                </TableBody>
                                            </StyledTable>
                                        </TableContainer> : <></>
                                }
                                {
                                    representation == 50 || representation >= 100 ?<>

                                        <h2 style={{ textAlign: "left" }}  sx={{mt : 10}}>Représentation graphique</h2>
                                        <Bar data={{
                                            labels: heur,
                                            datasets :statData
                                        }} options={options}
                                        ></Bar>
                                    </> : <></>
                                }
                            </TabPanel>
                            <TabPanel value="3">
                                <Facture facture={stat.facture}/>
                            </TabPanel>
                        </TabContext>
                    </Paper>
                </Box>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} >

                <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>
                    <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>
                        <Grid item xs={12} md={12}>
                            <Grid item xs={12} md={6}>
                                <StyledButton2 variant="contained"  color="primary" onClick={() => {
                                    window.location.replace(linkPdf+"/pdf");
                                }}>
                                    Exporter pdf
                                </StyledButton2>
                                <StyledButton2 variant="contained"  color="primary" onClick={handleOpen}>
                                    <Icon>send</Icon> Envoyer par email
                                </StyledButton2>
                            </Grid>

                        </Grid>
                    </Paper>

                </Box>
            </Grid>
        </>
    )
}
export default SimulationResult;