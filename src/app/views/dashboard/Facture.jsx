import {Grid, styled, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import React, {Fragment} from "react";
import * as PropTypes from "prop-types";
import HTools from "../HTools";

const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    minWidth : 750,
    width: 'auto',
    size :"small",
    ariaLabel :"a dense table",
    // overflowX : "auto",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const Facture = (props) => {
    return (
        <Fragment>
            <Grid item lg={12} md={12} sm={12} xs={12} >
                <h2 style={{ textAlign: "center" , color:"#ff621a"}}>Facturation de l'électricité consommée basé sur le tarif de JIRAMA</h2>

                <TableContainer>
                    <StyledTable style={{minWidth : 750,width: '100%'}} sx={{mb : 3}}>
                        <TableHead>
                            <TableRow >
                                <TableCell align="left"></TableCell>
                                <TableCell align="left">Quantité</TableCell>
                                <TableCell align="left">P.U.</TableCell>
                                <TableCell align="left">Montant(Ariary)</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                props.facture.representation.map((rep, index) => (
                                    <Fragment>
                                        <TableRow>
                                            <TableCell align="left" sx={{
                                                fontWeight : rep.total? 'bold' : '',
                                            }}>
                                                {rep.intitile}
                                            </TableCell>
                                            <TableCell align="left" sx={{
                                                fontWeight : rep.total? 'bold' : '',
                                            }}>
                                                {rep.quantite !== ''? HTools.formatNumber(parseFloat(rep.quantite)) : ''}
                                            </TableCell>
                                            <TableCell align="left" sx={{
                                                fontWeight : rep.total? 'bold' : '',
                                            }}>
                                                {rep.pu !== ''? HTools.formatNumber(rep.pu) : ''}
                                            </TableCell>
                                            <TableCell align="left" sx={{
                                                fontWeight : rep.total? 'bold' : '',
                                            }}>
                                                {HTools.formatNumber(rep.montant) }
                                            </TableCell>
                                        </TableRow>
                                    </Fragment>
                                ))
                            }
                        </TableBody>
                    </StyledTable>
                </TableContainer>
                <p style={{
                    fontWeight : 'bold',
                }}>** {HTools.spellAriary(props.facture.pTotal)} **</p>
            </Grid>

        </Fragment>
        )

}
export default Facture;