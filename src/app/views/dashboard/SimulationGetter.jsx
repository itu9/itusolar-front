import Simulation from "./Simulation";
import {useParams} from "react-router-dom";

const SimulationGetter = () => {
    const {id} = useParams();
    return (
        <Simulation scheduleId={id}></Simulation>
    )
}
export default SimulationGetter