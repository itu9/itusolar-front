import { PDFViewer } from '@react-pdf/renderer';
import PredictionContentPdf from "./PredictionContentPdf";
import HButton from "../components/HButton";
import ReactPDF from '@react-pdf/renderer'
const PredictionPdf = () => {
    return (
        <>
        <PDFViewer width={"500x" } style={{
            marginTop : '50px',
        }}>
            <PredictionContentPdf></PredictionContentPdf>
        </PDFViewer>
        </>
    )
}
export default PredictionPdf;