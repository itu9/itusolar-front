import { Card, Grid, styled, useTheme } from '@mui/material';
import {Fragment, useEffect, useState} from 'react';
import StatCards from './shared/StatCards';
import SectionState from "./shared/SectionState";
import {baseUrl} from "../../utils/constant";
import HTools from "../HTools";

const ContentBox = styled('div')(({ theme }) => ({
  margin: '30px',
  [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const Analytics = () => {
  const { palette } = useTheme();
    const [datas, setDatas] = useState(HTools.dashboardModel);
    useEffect(() => {
        let url = baseUrl+'/historique/init'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);

    return (
    <Fragment>
      <ContentBox className="analytics">
        <Grid container spacing={3}>
          <Grid item lg={12} md={12} sm={12} xs={12}>
              <Grid item xs={12} md={12} key={-1}>
                  <h1>Production et consommation d'énergie</h1>
              </Grid>
          </Grid>

            <Grid item xs={12} md={12}>
                    <h2 style={{ textAlign: "left" , color:"#1976d2"}}>Etat par section des sources solaires</h2>
            </Grid>
            {
                datas.sectionStates.map((data,index) => (
                    <SectionState sizeMax={6} sizeMin={12} key={index} data={data} />
                ))
            }
            <Grid item lg={12} md={12} sm={12} xs={12}>
                <StatCards data={datas}  />
            </Grid>
        </Grid>
      </ContentBox>
    </Fragment>
  );
};

export default Analytics;
