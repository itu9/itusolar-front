import {baseUrl, hc_dark} from "../../utils/constant";
import React, {useEffect, useState} from "react";
import {
    Box, Button,
    Grid,
    Icon,
    IconButton,
    Paper,
    styled, Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import HTools from "../HTools";
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    minWidth : 750,
    width: 'auto',
    size :"small",
    ariaLabel :"a dense table",
    // overflowX : "auto",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const StyledButton = styled(Button)(({ theme }) => ({
    margin: theme.spacing(1),
}));
const SimulationList = () => {
    const [datas, setDatas] = useState({
        datas : []
    });
    useEffect(() => {
        let url = baseUrl+'/schedule/all'
        let params = {
        }
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log(response)
                setDatas(response)
            });
    }, []);

    return (
        <>
            <ContentBox className="analytics">
                <Grid item lg={8} md={8} sm={12} xs={12} >
                    <h1 style={{color : hc_dark}}>Liste des simulations enregistrées </h1>
                    <Grid item lg={12} md={12} sm={12} xs={12} >
                        <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>
                            <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>
                                <TableContainer>
                                    <StyledTable style={{minWidth : 750,width: '100%'}}>
                                        <TableHead>
                                            <TableRow >
                                                <TableCell align="left">Titre</TableCell>
                                                <TableCell align="left">Debut</TableCell>
                                                <TableCell align="left">Fin</TableCell>
                                                <TableCell align="left"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                datas.datas.map((data,index) => (
                                                    <TableRow key={index}>
                                                        <TableCell align="left">
                                                            {data.titre}
                                                        </TableCell>
                                                        <TableCell align="left">
                                                            {HTools.formatDate(data.startdate)}
                                                        </TableCell>
                                                        <TableCell align="left">
                                                            {HTools.formatDate(data.enddate)}
                                                        </TableCell>
                                                        <TableCell align="left">
                                                            <StyledButton  variant="contained" color="primary"
                                                                           onClick={() => {
                                                                               window.location.replace('/dashboard/simulation/'+data.id)
                                                                           }}
                                                            >Charger </StyledButton>
                                                        </TableCell>
                                                    </TableRow>
                                                ))
                                            }
                                        </TableBody>
                                    </StyledTable>
                                </TableContainer>
                            </Paper>
                        </Box>
                    </Grid>
                </Grid>
            </ContentBox>
        </>
    );
}
export default SimulationList;