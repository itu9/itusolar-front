import {
    Box,
    Button,
    Grid,
    Icon,
    IconButton,
    Paper,
    styled,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextareaAutosize,
    TextField,
} from "@mui/material";
import {baseUrl, hc_dark} from "../../utils/constant";
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import React, {Fragment, useEffect, useRef, useState} from 'react';
import Backdrop from '@mui/material/Backdrop';
import HTools from "../HTools";
import SimulationResult from "./SimulationResult";
import HAutoCompleted from "../components/HAutoCompleted";

const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    minWidth : 750,
    width: 'auto',
    size :"small",
    ariaLabel :"a dense table",
    // overflowX : "auto",
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));

const sources = [
    {
        id : 0,
        label : 'Solaire'
    },
    {
        id : 50,
        label : 'JIRAMA'
    }
]
const BtnAddLigne = styled(Button)(({ theme }) => ({
    marginTop: theme.spacing(1),
}));
const StyledButton = styled(Button)(({ theme }) => ({
    margin: theme.spacing(1),
}));

const StyledBModal = styled(Button) (({ theme }) => ({
    width : '100%',
    marginTop: theme.spacing(2)
}))
const styleModal = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};
const dateStyle = {
    fontSize: '15px',
    width : '250px',
    height : '53px',
    marginTop: '1px',
    border: '#d9d9d9 solid 2px',
    borderRadius: '4px'
};

const Simulation = ({scheduleId}) => {
    const [id,setId] = useState(0);
    const [details,setDetails] = useState([]);
    const [utils, setUtils] = useState({
        appareils : [],
        lieux : [],
        sections : [],
        schedules : []
    })
    const template = {
        id : id,
        idValue : -1,
        nb : 0,
        appareilid : '',
        appareilidValue : -1,
        debut : null,
        fin : null,
        lieu : '',
        lieuValue : -1,
        source : '',
        sourceValue : -1,
        changed : false
    }
    const title = useRef(null);
    const startDate = useRef(null);
    const endDate = useRef(null);
    const interval = useRef(null);
    const [defaultTitle, setDefaultTitle] = useState(null);
    const [defaultStartDate, setDefaultStartDate] = useState(null);
    const [defaultEndDate, setDefaultEndDate] = useState(null);
    const [defaultInterval, setDefaultInterval] = useState(null);
    const [open, setOpen] = React.useState(false);
    const [creationState, changeCreationState] = React.useState(false);
    const [stat, setStat] = React.useState(HTools.statVoid)
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const addDetail = (index) => {
        addOnDetails(index,template);
    }
    const addOnDetails = (index, data) => {
        if (details.length > 0 && details.length-1 !== index)
            return;
        let response = [
            ...details,
            data
        ]
        setDetails(response)
        setId(id+1)
    }
    const removeOnVoid = (index) => {
        let res =[...details]
        if (isVoid(res[index]))
            removeDetails(index+1)
    }
    const isVoid = (row) => {
        return !row.changed
    }
    const removeDetails = (index) => {
        let res =[...details]
        res.splice(index, 1)
        setDetails(res)
    }
    const handleChange = (index,event) => {
        handleChangeValue(index,event,event.target.value)
    }
    const handleDate = (index,event,value) => {
        value = value!== ""?value+":00.000" : null
        value = value!== null ? HTools.prepareDate(value) : null
        handleChangeValue(index,event,value)
    }
    const handleChangeValue = (index,event,value) => {
        let result = [...details]
        result[index][event.target.name] = value
        result[index]['changed'] = true
        setDetails(result)
        sendData(result)
    }
    const sendData = (data) => {
        console.log("haha",data)
        let url = baseUrl+"/simulation/direct"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                datas: data
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setStat(response)
            });
    }
    const handleLoad = () => {
        let url = baseUrl+"/simulation/utils"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                id : scheduleId
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setUtils(response)
            });
    }
    useEffect(()=> {
        handleLoad()
        addDetail(0)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
    useEffect(() => {
        console.log('Utils modified')
        if (utils.schedules.length > 0) {
            let datas = [
                ...utils.schedules[0].datas,
                template
            ]
            setDetails(datas)
            sendData(datas)
            setId(datas.length)
            setDefaultTitle(utils.schedules[0].titre)
            let startTemp = utils.schedules[0].startdate;
            if (startTemp) {
                startTemp = startTemp.split(" ")[0]
            }
            setDefaultStartDate(startTemp)
            let endTemp = utils.schedules[0].enddate;
            if (endTemp) {
                endTemp = endTemp.split(" ")[0]
            }
            setDefaultEndDate(endTemp)
            setDefaultInterval(utils.schedules[0].interval)
        }
    }, [utils]);


    const save = () => {
        let url = baseUrl+'/schedule/create'
        let params = {
            schedule :{
                titre : title.current.value,
                startdate : startDate.current.value+' 00:00:00.000',
                enddate : endDate.current.value+' 00:00:00.000',
                interval : interval.current.value,
                userid : 1060, // USERMODIF
                id : scheduleId
            },
            details : details
        }
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                changeCreationState(false)
            });
        //
    }
    const openCreation = () => {
        changeCreationState(true)
    }
    return (

        <Fragment>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                    backdrop: {
                        timeout: 500,
                    },
                }}
            >
                <Fade  in={open}>
                    <Box sx={styleModal}>
                        <h1 >Envoie d'email</h1>
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Email</h3>
                        <TextField
                            style={{width:"100%"}}
                            type="email"
                            defaultValue={"rbaovola@yahoo.com"}
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Cc</h3>
                        <TextField
                            style={{width:"100%"}}
                            type="email"
                            defaultValue={"rnary@yahoo.com,solo@gmail.com"}
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Objet</h3>
                        <TextField
                            style={{width:"100%"}}
                            defaultValue={"Simulation"}
                            type="text"
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Corps</h3>
                        <TextareaAutosize style={{width:"100%",height:"100px"}}></TextareaAutosize>
                        <StyledBModal variant="contained"  color="primary" onClick={handleOpen}>
                            <Icon>send</Icon> Envoyer
                        </StyledBModal>
                    </Box>
                </Fade>
            </Modal>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={creationState}
                onClose={()=>{changeCreationState(false)}}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                    backdrop: {
                        timeout: 500,
                    },
                }}
            >
                <Fade  in={creationState}>
                    <Box sx={styleModal}>
                        <h1>Sauvegarder la simulation</h1>
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Titre</h3>
                        <TextField
                            style={{width:"100%"}}
                            type="text"
                            defaultValue={defaultTitle}
                            inputRef={title}
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Intervalle de répétition</h3>
                        <TextField
                            style={{width:"100%"}}
                            type="text"
                            defaultValue={defaultInterval}
                            inputRef={interval}
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Début</h3>
                        <TextField
                            style={{width:"100%"}}
                            type="date"
                            inputRef={startDate}
                            defaultValue={defaultStartDate}
                        />
                        <h3 style={{ textAlign: "left" , color:"#1976d2"}}>Fin</h3>
                        <TextField
                            style={{width:"100%"}}
                            defaultValue={defaultEndDate}
                            inputRef={endDate}
                            type="date"
                        />
                        <StyledBModal variant="contained"  color="primary" onClick={save}>
                            Sauvegarder
                        </StyledBModal>
                    </Box>
                </Fade>
            </Modal>
            <ContentBox className="analytics">
                <Grid item lg={8} md={8} sm={12} xs={12} >
                    <h1 style={{color : hc_dark}}>Simulation </h1>
                    <StyledButton variant="outlined" color="primary" onClick={
                        () => {
                            window.location.replace("/dashboard/simulation/list"
                            )
                        }}>
                        Voir les simulations enregistrées
                    </StyledButton>
                    <StyledButton variant="contained" color="success" onClick={
                        () => {
                            openCreation()

                        }}>
                        Enregistrer
                    </StyledButton>
                    <StyledButton variant="contained" color="success" onClick={
                        () => {
                            window.location.replace('/material/form/-1')
                        }}>
                        Ajouter un Appareils
                    </StyledButton>
                    <Grid item lg={12} md={12} sm={12} xs={12} >
                        <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>
                            <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>
                                <form onSubmit={e => {e.preventDefault()}}>
                                <TableContainer>
                                <StyledTable>
                                    <TableHead>
                                        <TableRow >
                                            <TableCell align="left">Nombre</TableCell>
                                            <TableCell align="left">Appareil</TableCell>
                                            <TableCell align="left">Début</TableCell>
                                            <TableCell align="left">Fin</TableCell>
                                            <TableCell align="left">Source</TableCell>
                                            <TableCell align="left">Lieu</TableCell>
                                            <TableCell align="left"></TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {details?
                                            details.map((detail,index) => (
                                                <>
                                                    <TableRow >
                                                        <TableCell align="right">
                                                            <TextField
                                                                sx={{ width: '80px'}}
                                                                value = {detail.nb}
                                                                name={"nb"}
                                                                type="number"
                                                                onChange={event => handleChange(index,event)}
                                                                onClick={() => {
                                                                    addDetail(index)
                                                                }}
                                                                onBlur={() => {
                                                                    removeOnVoid(index)
                                                            }
                                                            }
                                                            />
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            <HAutoCompleted
                                                                index={index}
                                                                name={"appareilid"}
                                                                datas={utils.appareils}
                                                                value={detail.appareilid}
                                                                label={"Appareils"}
                                                                removeOnVoid={(index) => {
                                                                    removeOnVoid(index)
                                                                }}
                                                                onClick={(index)=>{
                                                                    addDetail(index)
                                                                }}
                                                                handleChange={(index,event,value)=>{
                                                                    handleChangeValue(index,event,value)
                                                                }}
                                                            />
                                                        </TableCell>
                                                        <TableCell align="left" >
                                                            <input type={"datetime-local"}
                                                                   value={detail.debut}
                                                                   onBlur={() => {
                                                                       removeOnVoid(index)
                                                                   }
                                                                   }
                                                                   onClick={() => {
                                                                        addDetail(index)
                                                                    }}
                                                                   onChange={event => {
                                                                                handleDate(index,event,event.target.value)
                                                                           }
                                                                    }
                                                                   name={"debut"} style={dateStyle}/>
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            <input type={"datetime-local"}
                                                                   value={detail.fin}
                                                                   onBlur={() => {
                                                                       removeOnVoid(index)
                                                                   }
                                                                   }
                                                                   onClick={() => {
                                                                       addDetail(index)

                                                                   }}
                                                                   onChange={event => {
                                                                       handleDate(index,event,event.target.value)

                                                                   }}
                                                                   name={"fin"} style={dateStyle}/>
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            <HAutoCompleted
                                                                index={index}
                                                                name={"source"}
                                                                datas={sources}
                                                                value={detail.source}
                                                                label={"Sources"}
                                                                removeOnVoid={(index) => {
                                                                    removeOnVoid(index)
                                                                }}
                                                                onClick={(index)=>{
                                                                    addDetail(index)
                                                                }}
                                                                handleChange={(index,event,value)=>{
                                                                    handleChangeValue(index,event,value)
                                                                }}
                                                            />
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            <HAutoCompleted
                                                                index={index}
                                                                name={"lieu"}
                                                                datas={utils.lieux}
                                                                value={detail.lieu}
                                                                label={"Lieux"}
                                                                removeOnVoid={(index) => {
                                                                    removeOnVoid(index)
                                                                }}
                                                                onClick={(index)=>{
                                                                    addDetail(index)
                                                                }}
                                                                handleChange={(index,event,value)=>{
                                                                    handleChangeValue(index,event,value)
                                                                }}
                                                            />

                                                        </TableCell>
                                                        <TableCell align="right">
                                                            <IconButton onClick={() => {removeDetails(index)}}>
                                                                <Icon color="error">close</Icon>
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                </>
                                            )):<></>
                                        }

                                    </TableBody>
                                </StyledTable>
                                </TableContainer>
                                </form>
                                {
                                    details.length === 0?

                                        <BtnAddLigne  variant="contained"  color="primary"
                                        onClick={() => {
                                        addDetail(0)}
                                        }>
                                            <strong>+</strong> Ajouter une ligne
                                        </BtnAddLigne>: <></>
                                }
                            </Paper>

                        </Box>
                    </Grid>
                    <SimulationResult stat={stat} utils={utils}></SimulationResult>

                    {/*    <Grid item lg={12} md={12} sm={12} xs={12} >*/}

                    {/*        <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>*/}
                    {/*            <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>*/}
                    {/*                <Grid item xs={12} md={12}>*/}
                    {/*                    <Grid item xs={12} md={6}>*/}
                    {/*                        <StyledButton2 variant="contained"  color="primary">*/}
                    {/*                            Exporter pdf*/}
                    {/*                        </StyledButton2>*/}
                    {/*                        <StyledButton2 variant="contained"  color="primary" onClick={handleOpen}>*/}
                    {/*                            <Icon>send</Icon> Envoyer par email*/}
                    {/*                        </StyledButton2>*/}
                    {/*                    </Grid>*/}

                    {/*                </Grid>*/}
                    {/*            </Paper>*/}

                    {/*        </Box>*/}
                    {/*    </Grid>*/}
                </Grid>
            </ContentBox>
        </Fragment>
    )
}
export default Simulation;