import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import HTable from "../components/HTable";
import {TableCell} from "@mui/material";
const styles = StyleSheet.create({
    page: {
        flexDirection: 'row',
        backgroundColor: '#E4E4E4'
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    }
});

const PredictionContentPdf = () => {
    return (
        <Document>
            <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                    {/*<HTable head={*/}
                    {/*    <>*/}
                    {/*        <TableCell>Nombre</TableCell>*/}
                    {/*        <TableCell>Appareil</TableCell>*/}
                    {/*    </>*/}
                    {/*}></HTable>*/}
                </View>
            </Page>
        </Document>
    )
}
export default PredictionContentPdf;