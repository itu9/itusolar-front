import {
    Box,
    Button,
    Grid, Paper,
    styled,
    Table
} from "@mui/material";
import 'react-calendar/dist/Calendar.css';
import {Fragment, useEffect, useRef, useState} from 'react';
import {baseUrl, hc_dark} from '../../utils/constant'
import Calendrier from "./Calendrier";
import HTools from "../HTools";
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const Consommation = () => {
    const [events, setEvents] = useState({
        parameter : {},
        prediction : HTools.statVoid,
        schedules : []
    });
    const loadData = (startDate,endDate) => {
        let url = baseUrl+"/schedule/calendar"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                debut : startDate,
                fin : endDate
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setEvents(response)
            });
    }
    return (

        <Fragment>
            <ContentBox className="analytics">
                <Grid item lg={8} md={8} sm={12} xs={12} >
                    <h1 style={{color : hc_dark}}>Prédiction de calendrier </h1>
                    <Grid item lg={12} md={12} sm={12} xs={12} >
                        <Calendrier loadData={loadData} events={events}></Calendrier>

                    </Grid>
                </Grid>
            </ContentBox>
        </Fragment>
    )
}
export default Consommation;