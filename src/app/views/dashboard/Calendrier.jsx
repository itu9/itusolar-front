import {Box, Paper, styled} from "@mui/material";
import React, {useEffect, useRef, useState} from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import SimulationResult from "./SimulationResult";
import {baseUrl} from "../../utils/constant";

const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));

const Calendier = ({loadData,events}) => {
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [utils, setUtils] = useState({
        appareils : [],
        lieux : [],
        sections : []
    })
    const [section, setSection] = React.useState(-1);
    const calendarRef = useRef(null);
    useEffect(() => {
        if (startDate && endDate) {
            loadData(startDate,endDate);
        }
    }, [startDate,endDate]);

    const renderEventContent = (eventInfo) => {
        return (
            <div>
                <b>{eventInfo.timeText}</b>
                <i>{eventInfo.event.title}</i>
            </div>
        )
    }
    const handleDateClick = (dateClickInfo) => {
        //would like this to take me to dayGridDay view of this day
        calendarRef.current
            .getApi()
            .changeView('dayGridDay', dateClickInfo.date)
    }

    const handleLoad = () => {
        let url = baseUrl+"/simulation/utils"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setUtils(response)
                if (response.sections.length > 0) {
                    setSection(response.sections[0].id)
                }
            });
    }
    const handleDateSelect = (selectInfo) => {
        let title = prompt("Please enter a new title for your event");
        let calendarApi = selectInfo.view.calendar;

        calendarApi.unselect(); // clear date selection

        if (title) {
            calendarApi.addEvent({
                title,
                start: selectInfo.startStr,
                end: selectInfo.endStr,
                allDay: selectInfo.allDay
            });
        }
    }
    const handleEventClick= (clickInfo) => {

        // if (
        //     confirm(
        //         `Are you sure you want to delete the event '${clickInfo.event.title}'`
        //     )
        // ) {
        //     clickInfo.event.remove();
        // }
    };
    const handleEvents = (events) => {
        // this.setState({
        //     currentEvents: events
        // });
    };
    const prepareDate = (value) => {
        let date = value.getDate()
        let month = value.getMonth()+1
        let year = value.getFullYear();
        let result = year+"-"+month+"-"+date;
        console.log(result);
        return result
    }
    useEffect(()=> {
        handleLoad()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
    return (
        <>
            <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>
                <Paper style={{paddingTop:'30px',paddingLeft:'30px',paddingRight:'30px',paddingBottom:'30px'}}>
                    <FullCalendar
                        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                        headerToolbar={{
                            left: "prev,next today",
                            center: "title",
                            right: "timeGridWeek,timeGridDay"
                        }}
                        buttonText={{ // texte personnalisé pour chaque bouton
                            week: 'Semaine',
                            day: 'Jour',
                            today : `Aujourd'hui`
                        }}
                        locale={"fr"}
                        initialView="timeGridWeek"
                        editable={true}
                        selectable={true}
                        selectMirror={true}
                        dayMaxEvents={true}
                        events={events.schedules}
                        datesSet={(args) => {
                            setStartDate(prepareDate(args.start))
                            setEndDate(prepareDate(args.end))
                        }}
                        weekends={true}// alternatively, use the `events` setting to fetch from a feed
                        select={handleDateSelect}
                        eventContent={renderEventContent} // custom render function
                        eventClick={handleEventClick}
                        eventsSet={handleEvents} // called after events are initialized/added/changed/removed
                    />
                    <SimulationResult stat={events.prediction} utils={utils}></SimulationResult>
                </Paper>

            </Box>
        </>
    );
}
export default Calendier;