import {Grid, styled, TextField} from "@mui/material";
import {Fragment, useEffect, useState} from 'react';
import {hc_info,hc_dark} from '../../utils/constant'
import SimpleCard from "../../components/SimpleCard";
import {baseUrl} from "../../utils/constant";
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const Consommation = () => {
    const [h,setH] = useState(0)
    const [m,setM] = useState(0)
    const [consommation,setConsommation] = useState(5000);

    const [energy,setEnergy] = useState(30000);

    const evalTime = (setH,setM,energy,cons ) =>{
        let consMin = cons/60
        console.log('energy',energy)
        console.log('consMin',consMin)
        console.log('cons',cons)
        let minDuration = energy / consMin
        console.log('minDuration',minDuration)
        let m = minDuration % 60
        let h = parseInt(minDuration/60,10)
        setM(m)
        setH(h)
    }
    useEffect(()=> {
        evalTime(setH,setM,energy,consommation)

    },[])
    return (

        <Fragment>
            <ContentBox className="analytics">
                <Grid item lg={8} md={8} sm={12} xs={12} >
                    <h1 style={{color : hc_dark}}>Consommation </h1>
                    <Grid item lg={4} md={4} sm={12} xs={12} >
                    <SimpleCard >
                    <h3 style={{color:hc_info}}>Consommation actuelle</h3>
                        <p>Unité : WH</p>
                    <p><TextField defaultValue={consommation}/></p>
                    </SimpleCard>
                    </Grid>
                    <div style={{marginTop : '15px'}}>
                    <SimpleCard >
                    <h3 style={{color:hc_info}}>Temps restants</h3>
                    <p>{h}h {m}m</p>
                    </SimpleCard>
                    </div>
                </Grid>
            </ContentBox>
        </Fragment>
    )
}
export default Consommation;