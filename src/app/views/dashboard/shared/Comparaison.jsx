import HForm from "../../components/HForm";
import {useEffect, useState} from "react";
import {baseUrl} from "../../../utils/constant";
import HTools from "../../HTools";
import {Grid, TableCell, TableRow} from "@mui/material";
import HTable from "../../components/HTable";
import Heading from "../../components/Heading";
import {BarElement, CategoryScale, Chart, Legend, LinearScale, PointElement, Tooltip} from "chart.js";
import {Scatter} from "react-chartjs-2";

Chart.register(CategoryScale);
Chart.register(LinearScale);
Chart.register(BarElement);
Chart.register(PointElement);

Chart.register(Legend);
Chart.register(Tooltip);
const Comparaison = () => {
    const [datas, setDatas] = useState({
        evolutionStates : [],
        real : []
    });
    const loadData = (start, end) => {
        let url = baseUrl+'/historique/consumption/comparison'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                start : start,
                end : end
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }
    const getDate = () => {
        let date = new Date()
        let resp = HTools.monthList[date.getMonth()]
        console.log(resp)
        let year = date.getFullYear()
        resp = {
            start : year+resp.start,
            end : year+resp.end
        }
        return resp
    }
    useEffect(() => {
        let date = getDate()
        loadData(date.start, date.end)
        // loadData()
    }, []);
    const toData = (datas) => {
        let solairesData = []
        for (let i = 0; i < datas.length; i++) {
            solairesData = [
                ...solairesData,
                {
                    x : datas[i].index, y : datas[i].value
                }
            ]
        }
        return solairesData
    }

    const getdata = () => {
        let solaires = datas.evolutionStates
        let jirama = datas.real
        let solarDatas = toData(solaires)
        let jiramaDatas = toData(jirama)
        let result ={
            datasets: [
                {
                    label: 'Puissance prévue',
                    data: solarDatas,
                    backgroundColor: 'rgba(25, 118, 210, 1)',

                },
                {
                    label: 'Puissance réelle',
                    data: jiramaDatas,
                    backgroundColor: 'rgba(255, 99, 132, 1)',
                },
            ],
        };
        console.log(result)
        return result
    }
    const options = {
        scales: {
            y: {
                beginAtZero: true,
                title: {
                    display: true,
                    text: 'Puissance en Watt(W)'
                },
            },
            x : {
                title: {
                    display: true,
                    text: 'Date sur un mois en jour(j)'
                },
            }
        },
        plugins: {
            legend: {
                display: true,
                position: 'top',
                align: 'center',
                labels: {
                    usePointStyle: true,
                    pointStyle: 'circle',
                },
            },
        },
    };
    return (
        <HForm title={"Comparaison entre la consommation réelle et enregistrée"} size={12}>
            <Grid container>
                <Grid lg={6} md={6} xs={12} sm={12} >
                    <div style={{paddingRight : '20px'}}>
                        <Heading>Consommation prévue</Heading>
                        <HTable head={(
                            <>
                                <TableCell align={"left"}>Jour</TableCell>
                                <TableCell align={"left"}>Valeur</TableCell>
                            </>
                        )}>
                            {
                                datas.evolutionStates.map((evolution, index) => (
                                    <TableRow>
                                        <TableCell align={"left"}>{evolution.index}</TableCell>
                                        <TableCell align={"left"}>{HTools.formatNumber(evolution.value)}W</TableCell>
                                    </TableRow>
                                ))
                            }
                        </HTable>
                    </div>
                </Grid>
                <Grid lg={6} md={6} xs={12} sm={12}>
                    <div style={{paddingRight : '20px'}}>
                        <Heading>Consommation réelle</Heading>
                        <HTable head={(
                            <>
                                <TableCell align={"left"}>Jour</TableCell>
                                <TableCell align={"left"}>Valeur</TableCell>
                            </>
                        )}>
                            {
                                datas.real.map((evolution, index) => (
                                    <TableRow>
                                        <TableCell align={"left"}>{evolution.index}</TableCell>
                                        <TableCell align={"left"}>{HTools.formatNumber(evolution.value)}W</TableCell>
                                    </TableRow>
                                ))
                            }
                        </HTable>
                    </div>
                </Grid>
                <Grid lg={8} md={8} xs={12} sm={12}>
                    <Heading >Représentation graphique</Heading>
                    <Scatter options={options} data={getdata()} />
                </Grid>
            </Grid>
        </HForm>
    )
}
export default Comparaison;