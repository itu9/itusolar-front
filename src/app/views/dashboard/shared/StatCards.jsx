import {
  Box, Card, Grid, Icon, IconButton, styled, Table, Tooltip,
  TableCell,
  TableHead,
  TableRow, TableBody, Button,
} from '@mui/material';
import { Small } from 'app/components/Typography';
import {useNavigate} from "react-router-dom";
import Chart from "chart.js/auto";
import { useState } from "react";
import { Line,Bar } from "react-chartjs-2";
import StatLink from "./StatLink";
import HTools from "../../HTools";

const StyledCard = styled(Card)(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '24px !important',
  background: theme.palette.background.paper,
  [theme.breakpoints.down('sm')]: { padding: '16px !important' },
}));

const ContentBox = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center',
  '& small': { color: theme.palette.text.secondary },
  '& .icon': { opacity: 0.6, fontSize: '44px', color: theme.palette.primary.main },
}));

const Heading = styled('h6')(({ theme }) => ({
  margin: 0,
  marginTop: '4px',
  fontSize: '14px',
  fontWeight: '500',
  color: theme.palette.primary.main,
}));

const StatCards = ({data = HTools.dashboardModel}) => {

  const navigate = useNavigate();
  const cardList = [
    { name: 'Vitesse de production', amount: data.input+'KwH', icon: 'power', disabled : true,
    action : (item) => {

    }
    },
    { name: 'Vitesse de consommation',value : data.output+'kWH', amount: data.output+'KwH', icon: 'power' , disabled : true,
      action : (item) => {
        navigate('/details/consommation')
      }
    },
    { name: 'Enèrgies restantes', amount:data.batteryState+'Kw' , icon: 'battery_charging_full', disabled : true,
      action : (item) => {} },
    {
      blancked : true
    },
    { name: data.meteo.label, amount: data.meteo.temperature+'°C', icon: 'wb_sunny' ,  disabled : true,
      action : (item) => {}}
  ];
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
  }));

  var dataEnergies = [
    {
      id: 1,
      year: 2016,
      userGain: 80000,
      userLost: 823
    },
    {
      id: 2,
      year: 2017,
      userGain: 45677,
      userLost: 345
    },
    {
      id: 3,
      year: 2018,
      userGain: 78888,
      userLost: 555
    },
    {
      id: 4,
      year: 2019,
      userGain: 90000,
      userLost: 4555
    },
    {
      id: 5,
      year: 2020,
      userGain: 4300,
      userLost: 234
    }
  ];
  var jirama = [
    {
      id: 1,
      year: 2020,
      userGain: 80000,
      userLost: 823
    },
    {
      id: 2,
      year: 2021,
      userGain: 45677,
      userLost: 345
    }  ];
  var labels = dataEnergies.map((data) => data.year);
  labels = [
      ...labels,
      ...jirama.map((data) => data.year)
  ]
  const [energyChart, setenergyChart] = useState({
    labels: dataEnergies.map((data) => data.year),
    datasets: [
      {
        label: "Energies produites",
        data: dataEnergies.map((data) => data.userGain),
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 2
      }
    ]
  });
  const [consomChart, setconsomChart] = useState({
    labels: labels,
    datasets: [
      {
        label: "Consommation solaire ",
        data: dataEnergies.map((data) => data.userGain),
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 2
      },{
        label: "Consommation jirama",
        data: jirama.map((data) => data.userGain),
        borderColor: 'rgb(192, 75 , 192)',
        borderWidth: 2
      }
    ]
  });
  return (
      <>

        <Grid container spacing={3} sx={{ mb: '24px' }}>
          <Grid item xs={12} md={12}>
            <ContentBox>
              <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Etat global des sources solaires</h2>
            </ContentBox>
          </Grid>
          {cardList.map((item, index) => (
              <Grid item xs={12} md={4} key={index}>
                {
                  item.blancked? <></> : <StyledCard elevation={6}>
                    <ContentBox>
                      <Icon className="icon">{item.icon}</Icon>
                      <Box ml="12px">
                        <Small>{item.name}</Small>
                        <Heading>

                          {item.amount}</Heading>
                      </Box>
                    </ContentBox>
                    {
                      !item.disabled ? (
                          <Tooltip title="View Details" placement="top">
                            <IconButton onClick={() => {item.action(item)} }>
                              <Icon>arrow_right_alt</Icon>
                            </IconButton>
                          </Tooltip>
                      ) : <></>
                    }
                  </StyledCard>
                }

              </Grid>
          ))}
          {/* Consommation*/}
          <Grid item xs={12} md={12}>
            <ContentBox>
              <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Autres statistiques</h2>
            </ContentBox>
          </Grid>
          <StatLink item = {{icon:'trending_up', name : `Evolution de l'utilisation de chaque source`,disable : false,
            action:()=>{window.location.replace("/dashboard/evolution")}
          }}></StatLink>
          <StatLink item = {{icon:'warning', name : `Intervalles de consommation anormale`,disable : false}}></StatLink>
          <StatLink item = {{icon:'pie_chart', name : `Répartition des sources d'énergies`,disable : false,
          action: ()=>{
            window.location.replace('/dashboard/repartition')
          }}}></StatLink>
          <StatLink item = {{icon:'equalizer', name : `Comparaison entre la consommation réelle et enregistrée`,disable : false,
            action: ()=>{
              window.location.replace('/dashboard/comparaison')
            }
          }}></StatLink>
          {/*<Grid item xs={12} md={12}>*/}
          {/*  <ContentBox>*/}
          {/*    <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Consommation</h2>*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <ContentBox>*/}
          {/*    <Line*/}
          {/*        data={consomChart}*/}
          {/*        options={{*/}
          {/*          plugins: {*/}
          {/*            title: {*/}
          {/*              display: true,*/}
          {/*              text: "Consommation depuis 21 juillet 2023, 22 août 2023",*/}
          {/*            },*/}
          {/*            legend: {*/}
          {/*              display: false*/}
          {/*            }*/}
          {/*          }*/}
          {/*        }}*/}
          {/*    />*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <Box width="100%" overflow="auto" style={{marginTop : "50px"}}>*/}
          {/*    <StyledTable>*/}
          {/*      <TableHead>*/}
          {/*        <TableRow >*/}
          {/*          <TableCell align="left"></TableCell>*/}
          {/*          <TableCell align="left">Solaire</TableCell>*/}
          {/*          <TableCell align="left">Jirama</TableCell>*/}
          {/*        </TableRow>*/}
          {/*      </TableHead>*/}

          {/*      <TableBody>*/}
          {/*        <TableRow >*/}
          {/*          <TableCell align="left">Consommation totale</TableCell>*/}
          {/*          <TableCell align="left">40 KwH</TableCell>*/}
          {/*          <TableCell align="left">10 KwH</TableCell>*/}
          {/*        </TableRow>*/}
          {/*        <TableRow >*/}
          {/*          <TableCell align="left">Valeur (Ariary)</TableCell>*/}
          {/*          <TableCell align="left">400 000 ariary</TableCell>*/}
          {/*          <TableCell align="left">100 000 ariary</TableCell>*/}
          {/*        </TableRow>*/}
          {/*        <TableRow >*/}
          {/*        <TableCell align="left">Pourcentage</TableCell>*/}
          {/*          <TableCell align="left">80%</TableCell>*/}
          {/*          <TableCell align="left">20%</TableCell>*/}
          {/*      </TableRow>*/}
          {/*      </TableBody>*/}
          {/*    </StyledTable>*/}
          {/*  </Box>*/}
          {/*</Grid>*/}
          {/*/!* Energies restante*!/*/}
          {/*<Grid item xs={12} md={12}>*/}
          {/*  <ContentBox>*/}
          {/*    <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Energies restantes</h2>*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <ContentBox>*/}
          {/*    <Bar*/}
          {/*        data={energyChart}*/}
          {/*        options={{*/}
          {/*          plugins: {*/}
          {/*            title: {*/}
          {/*              display: true,*/}
          {/*              text: "Energies stoquées depuis 21 juillet 2023, 22 août 2023",*/}
          {/*            },*/}
          {/*            legend: {*/}
          {/*              display: false*/}
          {/*            }*/}
          {/*          }*/}
          {/*        }}*/}
          {/*    />*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={4}>*/}
          {/*  <Grid container spacing={3} sx={{ mb: '24px' }}>*/}

          {/*    <Grid item xs={12} md={12}>*/}
          {/*      <StyledCard elevation={6}>*/}
          {/*        <ContentBox>*/}
          {/*          <Icon className="icon">battery_std</Icon>*/}
          {/*          <Box ml="12px">*/}
          {/*            <Small>Reste d'énergie</Small>*/}
          {/*            <Heading>*/}

          {/*              210 Kw</Heading>*/}
          {/*          </Box>*/}
          {/*        </ContentBox>*/}
          {/*      </StyledCard>*/}
          {/*    </Grid>*/}
          {/*    <Grid item xs={12} md={12}>*/}
          {/*      <StyledCard elevation={6}>*/}
          {/*        <ContentBox>*/}
          {/*          <Icon className="icon" style={{color:"#B36B00"}}>monetization_on</Icon>*/}
          {/*          <Box ml="12px">*/}
          {/*            <Small>*/}
          {/*              <Tooltip title={"Valeur en ariary en cas de vente"} >*/}
          {/*                <Button style={{textAlign:'left'}}>Valeur en ariary</Button>*/}
          {/*              </Tooltip>*/}
          {/*              </Small>*/}
          {/*            <Heading>*/}

          {/*              800 000 ariary</Heading>*/}
          {/*          </Box>*/}
          {/*        </ContentBox>*/}
          {/*      </StyledCard>*/}
          {/*    </Grid>*/}
          {/*  </Grid>*/}
          {/*</Grid>*/}
          {/*/!* Energies stoquées*!/*/}
          {/*<Grid item xs={12} md={12}>*/}
          {/*  <ContentBox>*/}
          {/*    <h2 style={{ textAlign: "center" , color:"#1976d2"}}>Energies produites</h2>*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={8}>*/}
          {/*  <ContentBox>*/}
          {/*    <Line*/}
          {/*        data={energyChart}*/}
          {/*        options={{*/}
          {/*          plugins: {*/}
          {/*            title: {*/}
          {/*              display: true,*/}
          {/*              text: "Energies produites depuis 21 juillet 2023, 22 août 2023",*/}
          {/*            },*/}
          {/*            legend: {*/}
          {/*              display: false*/}
          {/*            }*/}
          {/*          }*/}
          {/*        }}*/}
          {/*    />*/}
          {/*  </ContentBox>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={4}>*/}
          {/*  <Grid container spacing={3} sx={{ mb: '24px' }}>*/}

          {/*    <Grid item xs={12} md={12}>*/}
          {/*      <StyledCard elevation={6}>*/}
          {/*        <ContentBox>*/}
          {/*          <Icon className="icon">battery_charging_full</Icon>*/}
          {/*          <Box ml="12px">*/}
          {/*            <Small>Total des énergies stoquées</Small>*/}
          {/*            <Heading>*/}

          {/*              210 Kw</Heading>*/}
          {/*          </Box>*/}
          {/*        </ContentBox>*/}
          {/*      </StyledCard>*/}
          {/*    </Grid>*/}
          {/*    <Grid item xs={12} md={12}>*/}
          {/*      <StyledCard elevation={6}>*/}
          {/*        <ContentBox>*/}
          {/*          <Icon className="icon" style={{color:"#B36B00"}}>monetization_on</Icon>*/}
          {/*          <Box ml="12px">*/}
          {/*            <Small>Valeur de gain</Small>*/}
          {/*            <Heading>*/}

          {/*              800 000 ariary</Heading>*/}
          {/*          </Box>*/}
          {/*        </ContentBox>*/}
          {/*      </StyledCard>*/}
          {/*    </Grid>*/}
          {/*  </Grid>*/}
          {/*</Grid>*/}
        </Grid>

      </>
  );
};

export default StatCards;
