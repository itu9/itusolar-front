import HForm from "../../components/HForm";
import {useEffect, useState} from "react";
import {baseUrl} from "../../../utils/constant";
import HTools from "../../HTools";
import {Grid, TableCell, TableRow} from "@mui/material";
import Heading from "../../components/Heading";
import HTable from "../../components/HTable";
import HContainer from "../../components/HContainer";
import {Pie} from "react-chartjs-2";
import {Chart, ArcElement, Legend, Tooltip} from "chart.js";

Chart.register(ArcElement);

Chart.register(Legend);
Chart.register(Tooltip);

const Repartition = () => {
    const [datas, setDatas] = useState({
        evolutionStates:[]
    });
    const [repartition, setRepartition] = useState({
        labels  : [],
        datasets : [
            {
                label : 'Repartition des sources sur la consommation',
                data : [],
                backgroundColor: [
                ],
                borderColor: [
                ],
                borderWidth: 1,
            }
        ]
    });
    const loadData = (start, end) => {
        let url = baseUrl+'/historique/repartition'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                start : start,
                end : end
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }
    const getDate = () => {
        let date = new Date()
        let resp = HTools.monthList[date.getMonth()]
        console.log(resp)
        let year = date.getFullYear()
        resp = {
            start : year+resp.start,
            end : year+resp.end
        }
        return resp
    }

    const prepareDatas = () => {
        let labels = [];
        let data = [];
        for (let i = 0; i < datas.evolutionStates.length ;i++) {
            labels = [
                ...labels,
                datas.evolutionStates[i].label,
            ]
            data = [
                ...data,
                datas.evolutionStates[i].value
            ]
        }
        let response = {
            labels  : labels,
            legend: { display: true, position: "right" },
            datasets : [
                {
                    label : 'Consommation en Watt(W)',
                    data : data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1,
                }
            ]
        }
        setRepartition(response)
    }

    useEffect(() => {
        let date = getDate()
        loadData(date.start, date.end)
        // loadData()
    }, []);
    useEffect(() => {
        prepareDatas();
    }, [datas]);

    return (
        <HForm title={"Répartition de chaque source de la consommation"} size={12}>
            <HContainer >
                <Grid lg={6} md={6} xs={12} sm={12}>
                    <Heading >Représentation numerique</Heading>
                    <HTable  head={
                        <>
                            <TableCell align={"left"}>Source</TableCell>
                            <TableCell align={"left"}>Valeur</TableCell>
                        </>
                    }>
                        {
                            datas.evolutionStates.map((evolution,index) => (
                                <TableRow>
                                    <TableCell align={"left"}>{evolution.label}</TableCell>
                                    <TableCell align={"left"}>{HTools.formatNumber(evolution.value) }W</TableCell>
                                </TableRow>
                            ))
                        }
                    </HTable>
                </Grid>
                <Grid lg={6} md={6} xs={12} sm={12}>
                    <Heading >Représentation graphique</Heading>
                    <Pie data={repartition}

                         options={
                            {
                               plugins: {
                                 title: {
                                   display: true,
                                   text: "Energies stoquées depuis 21 juillet 2023, 22 août 2023",
                                 },
                                 legend: {
                                   display: true
                                 }
                               }
                            }}
                         // options={{
                        // plugins: {
                        //     title: {
                        //         display: true,
                        //         text: "Consommation depuis 21 juillet 2023, 22 août 2023",
                        //     },
                        //     legend: {
                        //         display: false
                        //     }
                        // }
                    />
                </Grid>
            </HContainer>
        </HForm>
    )
}
export default Repartition;