import {Box, Grid, Icon, IconButton, Paper, styled, TableCell, TableRow, Tooltip} from "@mui/material";
import HContentBox from "../../components/HContentBox";
import HTitle from "../../components/HTitle";
import HTable from "../../components/HTable";
import Onduleur from "./Onduleur";
import {Small} from "../../../components/Typography";
import HButton from "../../components/HButton";
const ContentBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginBottom : '40px',
    '& small': { color: theme.palette.text.secondary },
    '& .icon': { opacity: 0.6, fontSize: '44px', color: theme.palette.primary.main },
}));

const Heading = styled('h6')(({ theme }) => ({
    margin: 0,
    marginTop: '4px',
    fontSize: '14px',
    fontWeight: '500',
    color: theme.palette.primary.main,
}));
const SectionState = ({sizeMax,sizeMin, data, isInverter = false}) => {
    return (
        <>

            <Grid item lg={sizeMax} md={sizeMax} sm={sizeMin} xs={sizeMin}>
                <Paper>
                    <div style={{paddingTop : '20px', paddingBottom : '20px'}}>
                    <HContentBox >
                        <HTitle>{data.section}</HTitle>
                        <Grid container>
                        <Grid item lg={5} md={5} sm={12} xs={12}>
                            <img src={"/assets/images/inverter"+(isInverter?'1' : '')+'.jpg'} width="100%" alt="" />
                        </Grid>
                        <Grid item lg={6} md={6} sm={12} xs={12}>

                            <ContentBox>
                                <Icon className="icon">power</Icon>
                                <Box ml="12px">
                                    <Small>Vitesse de production</Small>
                                    <Heading>{data.input}WH</Heading>
                                </Box>
                            </ContentBox>
                            <ContentBox>
                                <Icon className="icon">power</Icon>
                                <Box ml="12px">
                                    <Small>Vitesse de consommation</Small>
                                    <Heading>{data.output}WH</Heading>
                                </Box>
                            </ContentBox>
                            <ContentBox>
                                <Icon className="icon">battery_charging_full</Icon>
                                <Box ml="12px">
                                    <Small>Batterie</Small>
                                    <Heading>{data.batteryState}W</Heading>
                                </Box>
                            </ContentBox>
                            <ContentBox>
                                <Icon className="icon">battery_full</Icon>
                                <Box ml="12px">
                                    <Small>Capacité de stockage</Small>
                                    <Heading>{data.capacite}WH</Heading>
                                </Box>
                            </ContentBox>
                            {
                                !isInverter?
                                    <ContentBox>
                                        <Icon className="icon">timer</Icon>
                                        <Box ml="12px">
                                            <Small>Heur d'expiration</Small>
                                            <Heading>{data.time}</Heading>
                                        </Box>
                                    </ContentBox> : <></>

                            }
                        </Grid>
                        </Grid>
                        {
                            !isInverter?
                                <HButton title={"Voir plus"} color={"primary"} mTop={"5px"} variant={"outlined"} clickListener={() => {
                                    window.location.replace('/dashboard/section/'+data.sectionid)
                                }}/> : <></>
                        }
                        {/*<Grid container>*/}
                        {/*    <Onduleur/>*/}
                        {/*    <Onduleur/>*/}
                        {/*</Grid>*/}
                    </HContentBox>
                    </div>
                </Paper>
            </Grid>
        </>
    )
}
export default SectionState;
