import HTable from "../../components/HTable";
import {Grid, TableCell, TableRow} from "@mui/material";

const Onduleur = () => {
    return (
        <>
            <Grid item lg={6} md={6 } sm={12} xs={12}>
                <h3>Onduleur A</h3>
                <div style={{marginLeft : '10px'}}>
                    <HTable head={
                        <>
                        </>
                    }>
                        <TableRow>
                            <TableCell align={"left"}>Production</TableCell>
                            <TableCell align={"left"}>40kW</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align={"left"}>Consommation</TableCell>
                            <TableCell align={"left"}>40kW</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align={"left"}>Reste</TableCell>
                            <TableCell align={"left"}>40kW</TableCell>
                        </TableRow>
                    </HTable>
                </div>
            </Grid>
        </>
    )
}
export default Onduleur;