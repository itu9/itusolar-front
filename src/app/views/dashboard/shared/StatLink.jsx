import {Box, Card, Grid, Icon, IconButton, styled, Tooltip} from "@mui/material";
import {Small} from "../../../components/Typography";
const StyledCard = styled(Card)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '24px !important',
    background: theme.palette.background.paper,
    [theme.breakpoints.down('sm')]: { padding: '16px !important' },
}));

const ContentBox = styled(Box)(({ theme }) => ({
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    '& small': { color: theme.palette.text.secondary },
    '& .icon': { opacity: 0.6, fontSize: '44px', color: theme.palette.primary.main },
}));
const StatLink = ({item}) => {
    return (
        <>
            <Grid item xs={12} md={6} >
                <StyledCard elevation={6}>
                    <ContentBox>
                        <Icon className="icon">{item.icon}</Icon>
                        <Box ml="12px">
                            <Small>{item.name}</Small>
                        </Box>
                    </ContentBox>
                    {
                        !item.disabled ? (
                            <Tooltip title="View Details" placement="top">
                                <IconButton onClick={() => {item.action(item)} }>
                                    <Icon>arrow_right_alt</Icon>
                                </IconButton>
                            </Tooltip>
                        ) : <></>
                    }
                </StyledCard>
            </Grid>
        </>
    )
}
export default StatLink;