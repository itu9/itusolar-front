import HForm from "../../components/HForm";
import {useEffect, useState} from "react";
import {baseUrl} from "../../../utils/constant";
import {useParams} from "react-router-dom";
import SectionState from "./SectionState";
import {Grid} from "@mui/material";


const InverterState = () => {
    const {id} = useParams();
    const [datas, setDatas] = useState({
        sections : []
    });
    useEffect(() => {
        let url = baseUrl+'/historique/inverter'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({
                id : id
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                console.log('response',response)
                setDatas(response)
            });
    }, []);
    return (
        <HForm title={"Etat des onduleurs"} size={12}>
            <Grid container spacing={3}>
            {
                datas.sections.map(section => (
                    <SectionState data={section} sizeMin={12} sizeMax={6} isInverter={true}/>
                ))
            }
            </Grid>
        </HForm>
    )
}
export default InverterState;