import {baseUrl, hc_dark} from "../../utils/constant";
import React, {useEffect, useRef, useState} from "react";
import {Button, Grid, Paper, styled, TextField} from "@mui/material";
import HAutoCompleted from "../components/HAutoCompleted";
import HTextField from "../components/HTextField";
import Swal from "sweetalert2";

const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));

const StyledButton = styled(Button)(({ theme }) => ({
    width: '100%',
    marginTop : '20px'
}));
const Heading = styled("h3")(({ theme }) => ({
    textAlign: "left" ,
    color:"#1976d2",
    fontSize:'20px'
}))

const AddSource = () => {
    const title = useRef(null);
    const [sections, setSections] = useState([]);
    const [types, setTypes] = useState([]);
    const [type, setType] = useState('');
    const [typeValue, setTypeValue] = useState(-1);
    const [section, setSection] = useState('');
    const [sectionValue, setSectionValue] = useState(-1);

    const handleTypeChange = (index,event={target : {name : ''}}, value) => {
        let target = 'type'
        if (event.target.name === target) {
            setType(value)
        } else if (event.target.name === target+'Value') {
            setTypeValue(value)
        }
    }

    const handleSectionChange = (index,event={target : {name : ''}}, value) => {
        let target = 'section'
        if (event.target.name === target) {
            setSection(value)
        } else if (event.target.name === target+'Value') {
            setSectionValue(value)
        }
    }
    useEffect(() => {
        let url = baseUrl+"/source/form/content"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify({}),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setSections(response.sections)
                setTypes(response.types)
            });
    }, []);


    const save = ()=> {
        let params = {
            titre : title.current.value,
            typeid : typeValue,
            sectionid : sectionValue
        }
        let url = baseUrl+"/source/save"
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(params),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                window.location.replace("/jirama/sources")
            }).catch(err => {
            Swal.fire({
                title: "Oupss..",
                text: "Verifier vos donnés",
                icon: "error",
            })});
    }

    return (
        <>
            <ContentBox className="analytics">
                    <Grid container spacing={3}>
                        <Grid item lg={6} md={6} sm={12} xs={12}>
                                <h1 style={{color : hc_dark}}>Ajouter une source </h1>
                                <HTextField title={"Titre"} inputRef={title} defaultValue={""} ></HTextField>
                                <Heading>Type</Heading>
                                <HAutoCompleted
                                    index={0}
                                    width={'100%'}
                                    name={"type"}
                                    top={'0%'}
                                    datas={types}
                                    value={type}
                                    label={"Type de source"}
                                    handleChange={(index,event,value)=>{
                                        handleTypeChange(index,event,value)
                                    }}
                                    removeOnVoid={(index) => {

                                    }}
                                    onClick={(index)=>{

                                    }}
                                ></HAutoCompleted>
                            <Heading>Section</Heading>
                            <HAutoCompleted
                                index={0}
                                width={'100%'}
                                top={'0%'}
                                name={"section"}
                                datas={sections}
                                value={section}
                                label={"Sections"}
                                handleChange={(index,event,value)=>{
                                    handleSectionChange(index,event,value)
                                }}
                                removeOnVoid={(index) => {

                                }}
                                onClick={(index)=>{

                                }}
                            ></HAutoCompleted>
                                <StyledButton variant="contained" color="success" onClick={save}>
                                    Enregistrer
                                </StyledButton>
                        </Grid>
                    </Grid>
            </ContentBox>
        </>
    )
}
export default AddSource;