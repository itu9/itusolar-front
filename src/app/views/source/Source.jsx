import {
    Button,
    Grid, Icon, IconButton,
    Paper,
    styled, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow
} from '@mui/material';
import React, {Fragment, useEffect, useState} from 'react';

import {baseUrl} from "../../utils/constant";
import Swal from "sweetalert2";
import HContainer from "../components/HContainer";

const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    overflow : 'auto',
    "& thead": {
        "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } },
    },
    "& tbody": {
        "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } },
    },
}));
const ContentBox = styled('div')(({ theme }) => ({
    margin: '30px',
    [theme.breakpoints.down('sm')]: { margin: '16px' },
}));
const StyledButton = styled(Button)(({ theme }) => ({
    margin: theme.spacing(1),
}));

const Tarif = () => {
    const [data,setData] =useState(null);
    useEffect(() => {
        let url = baseUrl+'/source/pages'
        fetch(url,{
            crossDomain:true,
            method:'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }).then(response=>response.json())
            .then(response => {
                setData(response)
            });
    }, []);


    return (
        <Fragment>
            <ContentBox className="analytics">
                <Grid container spacing={3}>
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                        <h1>Les differentes sources d'informations</h1>
                        <StyledButton variant="contained" color="success" onClick={
                            () => {
                                window.location.replace("/jirama/sources/form"
                                )
                            }}>
                            Ajouter une source
                        </StyledButton>
                    </Grid>
                    <Grid item lg={8} md={8} sm={12} xs={12}>
                        <HContainer>
                            <h2>Source</h2>
                            {
                                data !== null && data !== undefined ? <>
                                    <StyledTable>
                                        <TableHead>
                                            <TableRow >
                                                <TableCell align="left">Titre</TableCell>
                                                <TableCell align="left">Type</TableCell>
                                                <TableCell align="left">Section</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                data.sources !== null && data.sources !== undefined ?
                                                    data.sources.map((source,index) => (
                                                        <TableRow >
                                                            <TableCell align="left">
                                                                {source.titre}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {source.type}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {source.section}
                                                            </TableCell>
                                                            {/*    <TableCe*/}
                                                        </TableRow>

                                                    )): <></>
                                            }
                                        </TableBody>
                                    </StyledTable>
                                </> : 'chargement...'
                            }
                        </HContainer>
                    </Grid>
                </Grid>
            </ContentBox>
        </Fragment>
    );
}

export default Tarif;