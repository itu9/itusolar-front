
export const navigations =  [
  { name: 'Tableau de bord', path: '/dashboard/default', icon: 'dashboard'

  },
  { name: 'Calendrier', path: '/dashboard/predictions', icon: 'dashboard'

  },
  { name: 'Simulation', path: '/dashboard/simulation', icon: 'dashboard'

  },
  { name: 'Détails', path: '/details', icon: 'dashboard',
    children: [
      { name: 'Onduleurs', iconText: 'SI', path: '/dashboard/section/-1' },
      { name: 'Evolution', iconText: 'SI', path: '/dashboard/evolution' },
      { name: 'Répartition', iconText: 'SI', path: '/dashboard/repartition' },
      { name: 'Consommation', iconText: 'SI', path: '/dashboard/comparaison' },
    ]
  },
  { name: 'Données', path: '/jirama', icon: 'dashboard',
    children: [
      { name: 'Historique', iconText: 'SI', path: '/jirama/history' },
      { name: 'Tarif Jirama', iconText: 'SI', path: '/jirama/pagetarif' },
      { name: 'Sources', iconText: 'SI', path: '/jirama/sources' },
      { name: 'Batterie', iconText: 'SI', path: '/battery' },
      { name: 'Appareils', iconText: 'SI', path: '/material' },
      { name: 'Batiments', iconText: 'SI', path: '/places' },
    ]

  },
  { label: 'PAGES', type: 'label' },
  {
    name: 'Authentification',
    icon: 'security',
    children: [
      { name: 'Se connecter', iconText: 'SI', path: '/session/signin' },
      { name: `S'inscrire`, iconText: 'SU', path: '/session/signup' },
      // { name: 'Forgot Password', iconText: 'FP', path: '/session/forgot-password' },
      // { name: 'Error', iconText: '404', path: '/session/404' }
    ]
  },
  { label: 'Components', type: 'label' },
  {
    name: 'Components',
    icon: 'favorite',
    badge: { value: '30+', color: 'secondary' },
    children: [
      { name: 'Auto Complete', path: '/material/autocomplete', iconText: 'A' },
      { name: 'Buttons', path: '/material/buttons', iconText: 'B' },
      { name: 'Checkbox', path: '/material/checkbox', iconText: 'C' },
      { name: 'Dialog', path: '/material/dialog', iconText: 'D' },
      { name: 'Expansion Panel', path: '/material/expansion-panel', iconText: 'E' },
      { name: 'Form', path: '/material/form', iconText: 'F' },
      { name: 'Icons', path: '/material/icons', iconText: 'I' },
      { name: 'Menu', path: '/material/menu', iconText: 'M' },
      { name: 'Progress', path: '/material/progress', iconText: 'P' },
      { name: 'Radio', path: '/material/radio', iconText: 'R' },
      { name: 'Switch', path: '/material/switch', iconText: 'S' },
      { name: 'Slider', path: '/material/slider', iconText: 'S' },
      { name: 'Snackbar', path: '/material/snackbar', iconText: 'S' },
      { name: 'Table', path: '/material/table', iconText: 'T' }
    ]
  },
  // {
  //   name: 'Charts',
  //   icon: 'trending_up',
  //   children: [{ name: 'Echarts', path: '/charts/echarts', iconText: 'E' }]
  // },
  // {
  //   name: 'Documentation',
  //   icon: 'launch',
  //   type: 'extLink',
  //   path: 'http://demos.ui-lib.com/matx-react-doc/'
  // }
];
