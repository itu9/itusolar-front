import { createContext, useEffect, useReducer } from 'react';
import axios from 'axios';
import { MatxLoading } from 'app/components';
import {baseUrl,loginUrl} from "../utils/constant";

const initialState = {
    user: null,
    isInitialised: false,
    isAuthenticated: false
};

// const isValidToken = (accessToken) => {
//   if (!accessToken) return false;

//   const decodedToken = jwtDecode(accessToken);
//   const currentTime = Date.now() / 1000;
//   return decodedToken.exp > currentTime;
// };

// const setSession = (accessToken) => {
//   if (accessToken) {
//     localStorage.setItem('accessToken', accessToken);
//     axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
//   } else {
//     localStorage.removeItem('accessToken');
//     delete axios.defaults.headers.common.Authorization;
//   }
// };

const reducer = (state, action) => {
    switch (action.type) {
        case 'INIT': {
            const { isAuthenticated, user } = action.payload;
            return { ...state, isAuthenticated, isInitialised: true, user };
        }

        case 'LOGIN': {
            const { user } = action.payload;
            return { ...state, isAuthenticated: true, user };
        }

        case 'LOGOUT': {
            return { ...state, isAuthenticated: false, user: null };
        }

        case 'REGISTER': {
            const { user } = action.payload;

            return { ...state, isAuthenticated: true, user };
        }

        default:
            return state;
    }
};

const AuthContext = createContext({
    ...initialState,
    method: 'JWT',
    login: () => {
    },
    logout: () => {},
    register: () => {}
});

export const AuthProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);


    const login = async (email, password,action,onError) => {
        let data = {
            identifiant : email,
            passe : password,
            interim : 0
        }
        console.log(data)
        let url = baseUrl+loginUrl;
        console.log(url);
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => {
                sessionStorage.setItem("token",JSON.stringify(data)) ;
                action(data)
                // console.log(data);
                // if (data.code === undefined || data.code === null ) {
                //   sessionStorage.setItem("token",JSON.stringify(data.data)) ;
                //   action(data)
                // } else {
                //   onError(data)
                // }
            }).catch(err => {
                onError(data)
            })
        // const { user } = response.data;

        // dispatch({ type: 'LOGIN', payload: { user } });
    };

    const register = async (email, username, password) => {
        const response = await axios.post('/api/auth/register', { email, username, password });
        const { user } = response.data;

        dispatch({ type: 'REGISTER', payload: { user } });
    };

    const logout = () => {
        dispatch({ type: 'LOGOUT' });
    };

    useEffect(() => {
        (async () => {
            try {
                const { data } = await axios.get('/api/auth/profile');
                dispatch({ type: 'INIT', payload: { isAuthenticated: true, user: data.user } });
            } catch (err) {
                console.error(err);
                dispatch({ type: 'INIT', payload: { isAuthenticated: false, user: null } });
            }
        })();
    }, []);

    // SHOW LOADER
    if (!state.isInitialised) return <MatxLoading />;

    return (
        <AuthContext.Provider value={{ ...state, method: 'JWT', login, logout, register }}>
            {children}
        </AuthContext.Provider>
    );
};

export default AuthContext;
